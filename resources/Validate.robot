*** Settings ***
Library  SeleniumLibrary
Library  String

*** Variables ***

*** Keywords ***
Text Should Be
    [Arguments]  ${msgLocator}  ${msg}
    Element Text Should Be  ${msgLocator}  ${msg}

Check amount of rows
    [Arguments]  ${tableLocation}  ${amontOfRows}
    Page Should Contain Element  ${tableLocation}  ${amontOfRows}

FieldNotEmpty
    [Arguments]  ${locator}
    Log To Console    \nTest1234
######################################################
ListNotEmpty
    [Arguments]  ${ListLocator}  ${submitLocator}  ${resultLocator}  ${result}
    Select From List By Value  ${ListLocator}  ${EMPTY}
    Click Element  ${submitLocator}
    Element Text Should Be  ${resultLocator}  ${result}

Check last digit
    [Arguments]  ${citizenId}  ${expected}
    ${result}=  Validate_check_last_digit_citizen_ID  ${citizenId}
    Should Be Equal  ${expected}  ${result}

Validate_check_last_digit_citizen_ID
    [Arguments]  ${IDCard}
    @{id}=  Split String To Characters  ${IDCard}
    ${last}=  BuiltIn.Evaluate  11-(13*@{id}[0] +12*@{id}[1] +11*@{id}[2] +10*@{id}[3] +9*@{id}[4] +8*@{id}[5] +7*@{id}[6] +6*@{id}[7] +5*@{id}[8] +4*@{id}[9] +3*@{id}[10]+2*@{id}[11])%11
    ${id13}=  Convert To Integer    @{id}[12]
    ${last}=  Convert To Integer    ${last}
    ${last}=   Run Keyword If    ${last}>=10    BuiltIn.Evaluate  abs(10-${last})  ELSE  Convert To Integer  ${last}
    ${result}=  Set Variable If    ${last}==${id13}  ${True}  ${False}
    Return From Keyword    ${result}

######################################################
