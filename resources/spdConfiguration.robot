*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}   http://192.168.2.16:9080
${UAT}  http://172.20.90.81:9080
${PRD}  http://intranet.sso.go.th
${LoginPage}	${MB}/spd/index.jsp?app=indexadmin
${URL}	    ${MB}/spd/
${Home}  ${URL}main.spd
${ADM0101}  ${URL}adm?app=ADM0101&job=init
${ADM0102}  ${URL}adm?app=ADM0102&job=init
${ADM0103}  ${URL}adm?app=ADM0103&job=init
${ADM0104}  ${URL}adm?app=ADM0104&job=init
${ADM0105}  ${URL}adm?app=ADM0105&job=init
${ADM0201}  ${URL}adm?app=ADM0201&job=init
${ADM0202}  ${URL}adm?app=ADM0202&job=init
${ADM0203}  ${URL}adm?app=ADM0203&job=init
${ADM0301}  ${URL}adm?app=ADM0301&job=init
${ADM0302}  ${URL}adm?app=ADM0302&job=init
${ADM0303}  ${URL}adm?app=ADM0303&job=init
${ADM0304}  ${URL}adm?app=ADM0304&job=init
${ADM0305}  ${URL}adm?app=ADM0305&job=init
${ADM0306}  ${URL}adm?app=ADM0306&job=init
${ADM0307}  ${URL}adm?app=ADM0307&job=init
${ADM0308}  ${URL}adm?app=ADM0308&job=init
${ADM0309}  ${URL}adm?app=ADM0309&job=init
${ADM0310}  ${URL}adm?app=ADM0310&job=init
${ADM0311}  ${URL}adm?app=ADM0311&job=init
${ADM0312}  ${URL}adm?app=ADM0312&job=init
${ADM0313}  ${URL}adm?app=ADM0313&job=init
${ADM0314}  ${URL}adm?app=ADM0314&job=init
${ADM0315}  ${URL}adm?app=ADM0315&job=init
${SPD0101}  ${URL}spd?app=SPD01001&job=init
${SPD0102}  ${URL}spd?app=SPD01002&job=init
${SPD0103}  ${URL}spd?app=SPD01003&job=init
${SPD0104}  ${URL}spd?app=SPD01004&job=init
${SPD0105}  ${URL}spd?app=SPD01005&job=init
${SPD0106}  ${URL}spd?app=SPD01006&job=init
${SPD0201}  ${URL}spd?app=SPD02001&job=init
${SPD0202}  ${URL}spd?app=SPD02002&job=init
${SPD0203}  ${URL}spd?app=SPD02003&job=init
${SPD0204}  ${URL}spd?app=SPD02004&job=init
${SPD0301}  ${URL}spd?app=SPD03001&job=init
${SPD0302}  ${URL}spd?app=SPD03002&job=init
${SPD0303}  ${URL}spd?app=SPD03003&job=init
${SPD0304}  ${URL}spd?app=SPD03004&job=init
${SPD0305}  ${URL}spd?app=SPD03005&job=init
${SPD0306}  ${URL}spd?app=SPD03006&job=init
${SPD0307}  ${URL}spd?app=SPD03007&job=init
${SPD0401}  ${URL}spd?app=SPD04001&job=init
${SPD0402}  ${URL}spd?app=SPD04002&job=init
${SPD0501}  ${URL}spd?app=SPD05001&job=init
${SPD0502}  ${URL}spd?app=SPD05002&job=init
${SPD0601}  ${URL}spd?app=SPD06001&job=init
${SPD0602}  ${URL}spd?app=SPD06002&job=init
${SPD0603}  ${URL}spd?app=SPD06003&job=init
${SPD0604}  ${URL}spd?app=SPD06004&job=init
${SPD0605}  ${URL}spd?app=SPD06005&job=init
${SPD0606}  ${URL}spd?app=SPD06006&job=init
${SPD0607}  ${URL}spd?app=SPD06007&job=init
${SPD0608}  ${URL}spd?app=SPD06008&job=init
${SPD0609}  ${URL}spd?app=SPD06009&job=init
${SPD0610}  ${URL}spd?app=SPD06010&job=init
${SPD0611}  ${URL}spd?app=SPD06011&job=init
${SPD0612}  ${URL}spd?app=SPD06012&job=init
${SPD07001}  ${URL}spd?app=SPD07001&job=init
${SPD07002}  ${URL}spd?app=SPD07002&job=init
${SPD0801}  ${URL}spd?app=SPD08001&job=init
${SPD0802}  ${URL}spd?app=SPD08002&job=init
${SPD0803}  ${URL}spd?app=SPD08003&job=init
${SPD0804}  ${URL}spd?app=SPD08004&job=init
${SPD0901}  ${URL}spd?app=SPD09001&job=init
${SPD1001}  ${URL}spd?app=SPD10001&job=init
${SPD1002}  ${URL}spd?app=SPD10002&job=init

*** Keywords ***
gotoProject&SelectModule
    [Arguments]  ${user}  ${module}
    Set Selenium Speed        0.5
    Open Browser    ${LoginPage}  gc
    #headlesschrome
    Maximize Browser Window

    Input Text    name:userid  ${user}
    Press Keys    name:userid  RETURN
    Click Button  ยอมรับ
    #ตั้งค่าระบบใช้ setup, ระบบบำนาญชราภาพใช้ spd
    Wait Until Page Contains Element   //button[@class="${module}-module"]
    Click Element  //button[@class="${module}-module"]

rowsOfSearchResult
    [Arguments]  ${rows}
    Element Should Contain  xpath://table[3]/tbody/tr/td[1]  ข้อมูลทั้งหมด ${rows} รายการ

SearchNotFound
    Element Should Contain  id:msg  ไม่พบข้อมูลที่ต้องการค้นหา กรุณาตรวจสอบเงื่อนไขที่ระบุ
