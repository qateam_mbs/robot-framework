*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}	http://192.168.2.14:9080
#${UAT}
#${PRD}
${LoginPage}	${MB}/nch/
${URL}	  ${MB}/nch/
${Home}	  ${URL}main.nch
${NCHDBM0101}	${URL}main.nch?module=dbm&app=NCHDBM0101&job=init
${NCHDBM0102}	${URL}main.nch?module=dbm&app=NCHDBM0102&job=init
${NCHDBM0103}	${URL}main.nch?module=dbm&app=NCHDBM0103&job=init
${NCHDIS0101}	${URL}main.nch?module=dis&app=NCHDIS0101&job=init
${NCHDIS0102}	${URL}main.nch?module=dis&app=NCHDIS0102&job=init
${NCHDIS0103}	${URL}main.nch?module=dis&app=NCHDIS0103&job=init
${NCHDIS0104}	${URL}main.nch?module=dis&app=NCHDIS0104&job=init
${NCHDIS0105}	${URL}main.nch?module=dis&app=NCHDIS0105&job=init
${NCHDIS0106}	${URL}main.nch?module=dis&app=NCHDIS0106&job=init
${NCHDRG0101}	${URL}main.nch?module=drg&app=NCHDRG0101&job=init
${NCHDRG0102}	${URL}main.nch?module=drg&app=NCHDRG0102&job=init
${NCHDRG0201}	${URL}main.nch?module=drg&app=NCHDRG0201&job=init
${NCHDRG0202}	${URL}main.nch?module=drg&app=NCHDRG0202&job=init
${NCHDRG0203}	${URL}main.nch?module=drg&app=NCHDRG0203&job=init
${NCHDRG0204}	${URL}main.nch?module=drg&app=NCHDRG0204&job=init
${NCHDRG0205}	${URL}main.nch?module=drg&app=NCHDRG0205&job=init
${NCHDRG0206}	${URL}main.nch?module=drg&app=NCHDRG0206&job=init
${NCHDRG0207}	${URL}main.nch?module=drg&app=NCHDRG0207&job=init
${NCHDRG0208}	${URL}main.nch?module=drg&app=NCHDRG0208&job=init
${NCHDRG0209}	${URL}main.nch?module=drg&app=NCHDRG0209&job=init
${NCHDRG0250}	${URL}main.nch?module=drg&app=NCHDRG0250&job=init
${NCHDRG0301}	${URL}main.nch?module=drg&app=NCHDRG0301&job=init
${NCHDRG0302}	${URL}main.nch?module=drg&app=NCHDRG0302&job=init
${NCHDRG0303}	${URL}main.nch?module=drg&app=NCHDRG0303&job=init
${NCHDRG0401}	${URL}main.nch?module=drg&app=NCHDRG0401&job=init
${NCHDRG0402}	${URL}main.nch?module=drg&app=NCHDRG0402&job=init
${NCHDRG0403}	${URL}main.nch?module=drg&app=NCHDRG0403&job=init
${NCHDRG0404}	${URL}main.nch?module=drg&app=NCHDRG0404&job=init
${NCHDRG0405}	${URL}main.nch?module=drg&app=NCHDRG0405&job=init
${NCHDRG0406}	${URL}main.nch?module=drg&app=NCHDRG0406&job=init
${NCHDRG0407}	${URL}main.nch?module=drg&app=NCHDRG0407&job=init
${NCHDRG0408}	${URL}main.nch?module=drg&app=NCHDRG0408&job=init
${NCHDRG0409}	${URL}main.nch?module=drg&app=NCHDRG0409&job=init
${NCHDRG0410}	${URL}main.nch?module=drg&app=NCHDRG0410&job=init
${NCHDRG0411}	${URL}main.nch?module=drg&app=NCHDRG0411&job=init
${NCHDRG0412}	${URL}main.nch?module=drg&app=NCHDRG0412&job=init
${NCHDRG0413}	${URL}main.nch?module=drg&app=NCHDRG0413&job=init
${NCHDRG0414}	${URL}main.nch?module=drg&app=NCHDRG0414&job=init
${NCHDRG0501}	${URL}main.nch?module=drg&app=NCHDRG0501&job=init
${NCHE720101}	${URL}main.nch?module=e72&app=NCHE720101&job=init
${NCHE720102}	${URL}main.nch?module=e72&app=NCHE720102&job=init
${NCHE720103}	${URL}main.nch?module=e72&app=NCHE720103&job=init
${NCHE720104}	${URL}main.nch?module=e72&app=NCHE720104&job=init
${NCHE720105}	${URL}main.nch?module=e72&app=NCHE720105&job=init
${NCHE720106}	${URL}main.nch?module=e72&app=NCHE720106&job=init
${NCHEMC0101}	${URL}main.nch?module=emco&app=NCHEMC0101&job=init
${NCHEMC0102}	${URL}main.nch?module=emco&app=NCHEMC0102&job=init
${NCHEMC0201}	${URL}main.nch?module=emco&app=NCHEMC0201&job=init
${NCHEMC0202}	${URL}main.nch?module=emco&app=NCHEMC0202&job=init
${NCHEMC0203}	${URL}main.nch?module=emco&app=NCHEMC0203&job=init
${NCHEMC0204}	${URL}main.nch?module=emco&app=NCHEMC0204&job=init
${NCHES20101}	${URL}main.nch?module=es2&app=NCHES20101&job=init
${NCHFCT0101}	${URL}main.nch?module=fct&app=NCHFCT0101&job=init
${NCHFCT0102}	${URL}main.nch?module=fct&app=NCHFCT0102&job=init
${NCHFCT0103}	${URL}main.nch?module=fct&app=NCHFCT0103&job=init
${NCHFCT0104}	${URL}main.nch?module=fct&app=NCHFCT0104&job=init
${NCHFCT0201}	${URL}main.nch?module=fct&app=NCHFCT0201&job=init
${NCHFCT0202}	${URL}main.nch?module=fct&app=NCHFCT0202&job=init
${NCHHIV0101}	${URL}main.nch?module=hiv&app=NCHHIV0101&job=init
${NCHHIV0102}	${URL}main.nch?module=hiv&app=NCHHIV0102&job=init
${NCHHIV0103}	${URL}main.nch?module=hiv&app=NCHHIV0103&job=init
${NCHHIV0104}	${URL}main.nch?module=hiv&app=NCHHIV0104&job=init
${NCHHIV0105}	${URL}main.nch?module=hiv&app=NCHHIV0105&job=init
${NCHHIV0106}	${URL}main.nch?module=hiv&app=NCHHIV0106&job=init
${NCHHIV0107}	${URL}main.nch?module=hiv&app=NCHHIV0107&job=init
${NCHHIV0108}	${URL}main.nch?module=hiv&app=NCHHIV0108&job=init
${NCHHIV0201}	${URL}main.nch?module=hiv&app=NCHHIV0201&job=init
${NCHHIV0202}	${URL}main.nch?module=hiv&app=NCHHIV0202&job=init
${NCHHMD0101}	${URL}main.nch?module=hmd&app=NCHHMD0101&job=init
${NCHHMD0102}	${URL}main.nch?module=hmd&app=NCHHMD0102&job=init
${NCHHMD0103}	${URL}main.nch?module=hmd&app=NCHHMD0103&job=init
${NCHHMD0201}	${URL}main.nch?module=hmd&app=NCHHMD0201&job=init
${NCHHMD0202}	${URL}main.nch?module=hmd&app=NCHHMD0202&job=init
${NCHMAS0102}	${URL}main.nch?module=master&app=NCHMAS0102&job=init
${NCHMAS0103}	${URL}main.nch?module=master&app=NCHMAS0103&job=init
${NCHMAS0104}	${URL}main.nch?module=master&app=NCHMAS0104&job=init
${NCHMAS0105}	${URL}main.nch?module=master&app=NCHMAS0105&job=init
${NCHMAS0106}	${URL}main.nch?module=master&app=NCHMAS0106&job=init
${NCHMAS0107}	${URL}main.nch?module=master&app=NCHMAS0107&job=init
${NCHMAS0108}	${URL}main.nch?module=master&app=NCHMAS0108&job=init
${NCHMAS0201}	${URL}main.nch?module=master&app=NCHMAS0201&job=init
${NCHMEP0101}	${URL}main.nch?module=mep&app=NCHMEP0101&job=init
${NCHMEP0102}	${URL}main.nch?module=mep&app=NCHMEP0102&job=init
${NCHMEP0103}	${URL}main.nch?module=mep&app=NCHMEP0103&job=init
${NCHMEP0104}	${URL}main.nch?module=mep&app=NCHMEP0104&job=init
${NCHMEP0201}	${URL}main.nch?module=mep&app=NCHMEP0201&job=init
${NCHMEP0202}	${URL}main.nch?module=mep&app=NCHMEP0202&job=init
${NCHMEP0203}	${URL}main.nch?module=mep&app=NCHMEP0203&job=init
${NCHMEP0204}	${URL}main.nch?module=mep&app=NCHMEP0204&job=init
${NCHMET0101}	${URL}main.nch?module=met&app=NCHMET0101&job=init
${NCHMET0102}	${URL}main.nch?module=met&app=NCHMET0102&job=init
${NCHMET0103}	${URL}main.nch?module=met&app=NCHMET0103&job=init
${NCHMET0104}	${URL}main.nch?module=met&app=NCHMET0104&job=init
${NCHMET0105}	${URL}main.nch?module=met&app=NCHMET0105&job=init
${NCHMET0106}	${URL}main.nch?module=met&app=NCHMET0106&job=init
${NCHOTP0101}	${URL}main.nch?module=otp&app=NCHOTP0101&job=init
${NCHOTP0102}	${URL}main.nch?module=otp&app=NCHOTP0102&job=init
${NCHOTP0103}	${URL}main.nch?module=otp&app=NCHOTP0103&job=init
${NCHOTP0104}	${URL}main.nch?module=otp&app=NCHOTP0104&job=init
${NCHOTP0105}	${URL}main.nch?module=otp&app=NCHOTP0105&job=init
${NCHOTP0106}	${URL}main.nch?module=otp&app=NCHOTP0106&job=init
${NCHOTP0107}	${URL}main.nch?module=otp&app=NCHOTP0107&job=init
${NCHOTP0201}	${URL}main.nch?module=otp&app=NCHOTP0201&job=init
${NCHPAY0101}	${URL}main.nch?module=pay&app=NCHPAY0101&job=init
${NCHPAY0102}	${URL}main.nch?module=pay&app=NCHPAY0102&job=init
${NCHPAY0103}	${URL}main.nch?module=pay&app=NCHPAY0103&job=init
${NCHPAY0202}	${URL}main.nch?module=pay&app=NCHPAY0202&job=init
${NCHPAY0203}	${URL}main.nch?module=pay&app=NCHPAY0203&job=init
${NCHPAY0204}	${URL}main.nch?module=pay&app=NCHPAY0204&job=init
${NCHPAY0206}	${URL}main.nch?module=pay&app=NCHPAY0206&job=init
${NCHPAY0207}	${URL}main.nch?module=pay&app=NCHPAY0207&job=init
${NCHPAY0208}	${URL}main.nch?module=pay&app=NCHPAY0208&job=init
${NCHPAY0210}	${URL}main.nch?module=pay&app=NCHPAY0210&job=init
${NCHPAY0302}	${URL}main.nch?module=pay&app=NCHPAY0302&job=init
${NCHPAY0303}	${URL}main.nch?module=pay&app=NCHPAY0303&job=init
${NCHPAY0304}	${URL}main.nch?module=pay&app=NCHPAY0304&job=init
${NCHPAY0306}	${URL}main.nch?module=pay&app=NCHPAY0306&job=init
${NCHPAY0307}	${URL}main.nch?module=pay&app=NCHPAY0307&job=init
${NCHPAY0308}	${URL}main.nch?module=pay&app=NCHPAY0308&job=init
${NCHPAY0310}	${URL}main.nch?module=pay&app=NCHPAY0310&job=init
${NCHPAY0402}	${URL}main.nch?module=pay&app=NCHPAY0402&job=init
${NCHPAY0403}	${URL}main.nch?module=pay&app=NCHPAY0403&job=init
${NCHPAY0404}	${URL}main.nch?module=pay&app=NCHPAY0404&job=init
${NCHPAY0406}	${URL}main.nch?module=pay&app=NCHPAY0406&job=init
${NCHPAY0407}	${URL}main.nch?module=pay&app=NCHPAY0407&job=init
${NCHPAY0408}	${URL}main.nch?module=pay&app=NCHPAY0408&job=init
${NCHPAY0410}	${URL}main.nch?module=pay&app=NCHPAY0410&job=init
${NCHPAY0502}	${URL}main.nch?module=pay&app=NCHPAY0502&job=init
${NCHPAY0503}	${URL}main.nch?module=pay&app=NCHPAY0503&job=init
${NCHPAY0504}	${URL}main.nch?module=pay&app=NCHPAY0504&job=init
${NCHPAY0506}	${URL}main.nch?module=pay&app=NCHPAY0506&job=init
${NCHPAY0507}	${URL}main.nch?module=pay&app=NCHPAY0507&job=init
${NCHPAY0508}	${URL}main.nch?module=pay&app=NCHPAY0508&job=init
${NCHPAY0510}	${URL}main.nch?module=pay&app=NCHPAY0510&job=init
${NCHPAY0601}	${URL}main.nch?module=pay&app=NCHPAY0601&job=init
${NCHPAY0701}	${URL}main.nch?module=pay&app=NCHPAY0701&job=init
${NCHPAY0801}	${URL}main.nch?module=pay&app=NCHPAY0801&job=init
${NCHPAY0901}	${URL}main.nch?module=pay&app=NCHPAY0901&job=init
${NCHPAY0902}	${URL}main.nch?module=pay&app=NCHPAY0902&job=init
${NCHPAY0903}	${URL}main.nch?module=pay&app=NCHPAY0903&job=init
${NCHPAY0904}	${URL}main.nch?module=pay&app=NCHPAY0904&job=init
${NCHPAY0905}	${URL}main.nch?module=pay&app=NCHPAY0905&job=init
${NCHPAY0906}	${URL}main.nch?module=pay&app=NCHPAY0906&job=init
${NCHPAY0907}	${URL}main.nch?module=pay&app=NCHPAY0907&job=init
${NCHPAY0908}	${URL}main.nch?module=pay&app=NCHPAY0908&job=init
${NCHPAY0909}	${URL}main.nch?module=pay&app=NCHPAY0909&job=init
${NCHPPS0101}	${URL}main.nch?module=pps&app=NCHPPS0101&job=init
${NCHPPS0102}	${URL}main.nch?module=pps&app=NCHPPS0102&job=init
${NCHPPS0103}	${URL}main.nch?module=pps&app=NCHPPS0103&job=init
${NCHPPS0104}	${URL}main.nch?module=pps&app=NCHPPS0104&job=init
${NCHPPS0201}	${URL}main.nch?module=pps&app=NCHPPS0201&job=init
${NCHPPS0202}	${URL}main.nch?module=pps&app=NCHPPS0202&job=init
${NCHRCP0101}	${URL}main.nch?module=rcp&app=NCHRCP0101&job=init
${NCHRCP0102}	${URL}main.nch?module=rcp&app=NCHRCP0102&job=init
${NCHRCP0103}	${URL}main.nch?module=rcp&app=NCHRCP0103&job=init
${NCHRCP0104}	${URL}main.nch?module=rcp&app=NCHRCP0104&job=init
${NCHRSK0101}	${URL}main.nch?module=rsk&app=NCHRSK0101&job=init
${NCHRSK0102}	${URL}main.nch?module=rsk&app=NCHRSK0102&job=init
${NCHRSK0201}	${URL}main.nch?module=rsk&app=NCHRSK0201&job=init
${NCHRSK0202}	${URL}main.nch?module=rsk&app=NCHRSK0202&job=init
${NCHRSK0203}	${URL}main.nch?module=rsk&app=NCHRSK0203&job=init
${NCHRSK0204}	${URL}main.nch?module=rsk&app=NCHRSK0204&job=init
${NCHSYS0102}	${URL}main.nch?module=system&app=NCHSYS0102&job=init
${NCHSYS0103}	${URL}main.nch?module=system&app=NCHSYS0103&job=init
${NCHSYS0104}	${URL}main.nch?module=system&app=NCHSYS0104&job=init
${NCHSYS0201}	${URL}main.nch?module=system&app=NCHSYS0201&job=init
${NCHSYS0202}	${URL}main.nch?module=system&app=NCHSYS0202&job=init
${NCHSYS0203}	${URL}main.nch?module=system&app=NCHSYS0203&job=init
${NCHSYS0301}	${URL}main.nch?module=system&app=NCHSYS0301&job=init
${NCHSYS0302}	${URL}main.nch?module=system&app=NCHSYS0302&job=init
${NCHSYS0401}	${URL}main.nch?module=system&app=NCHSYS0401&job=init
${NCHTKO0101}	${URL}main.nch?module=tko&app=NCHTKO0101&job=init
${NCHTKO0102}	${URL}main.nch?module=tko&app=NCHTKO0102&job=init
${NCHTMP0101}	${URL}main.nch?module=tmp&app=NCHTMP0101&job=init
${NCHTMP0102}	${URL}main.nch?module=tmp&app=NCHTMP0102&job=init
${NCHTMP0103}	${URL}main.nch?module=tmp&app=NCHTMP0103&job=init
${NCHTMP0104}	${URL}main.nch?module=tmp&app=NCHTMP0104&job=init
${NCHTMP0105}	${URL}main.nch?module=tmp&app=NCHTMP0105&job=init
${NCHUCEP0101}	${URL}main.nch?module=ucep&app=NCHUCEP0101&job=init
${NCHUCEP0102}	${URL}main.nch?module=ucep&app=NCHUCEP0102&job=init
${NCHUCEP0201}	${URL}main.nch?module=ucep&app=NCHUCEP0201&job=init
${NCHUCEP0202}	${URL}main.nch?module=ucep&app=NCHUCEP0202&job=init
${NCHUCEP0203}	${URL}main.nch?module=ucep&app=NCHUCEP0203&job=init
${NCHUCEP0204}	${URL}main.nch?module=ucep&app=NCHUCEP0204&job=init
${NCHUCEP0301}	${URL}main.nch?module=ucep&app=NCHUCEP0301&job=init
${NCHUCEP0302}	${URL}main.nch?module=ucep&app=NCHUCEP0302&job=init
${NCHUCEP0401}	${URL}main.nch?module=ucep&app=NCHUCEP0401&job=init
${NCHUCEP0901}	${URL}main.nch?module=ucep&app=NCHUCEP0901&job=init
${NCHUCEP0902}	${URL}main.nch?module=ucep&app=NCHUCEP0902&job=init

*** Keywords ***
gotoProject&SelectModule
    [Arguments]  ${user}  ${pass}  ${module}
    #Set Selenium Speed        0.5
    Open Browser    ${LoginPage}   gc
    Maximize Browser Window

    Input Text    name:userId  ${user}
    Input Password    name:password  ${pass}
    Press Keys    name:password  RETURN
    Click Button  ยอมรับ
    #----MODULE----#
    #Administrator/Import File Banks
    #Master
    #DRG	AdjRW >= 2
    #EMCO	ค่าบริการทางการแพทย์กรณีประสบอันตรายหรือเจ็บป่วยฉุกเฉิน 3 กองทุน (EMCO)
    #HIV	HIV
    #ปลูกถ่ายอวัยวะ     OTP
    #PAY        การเงิน
    #Temporary
    #เรียกเงินคืน
    #MET    สารเมทาโดน
    #แพทย์ผู้เชี่ยวชาญ+หมัน     MEP
    #ทุพพลภาพ    DIS
    #ฟอกเลือดด้วยเครื่องไตเทียม
    #ยาบัญชี จ (๒)
    #ส่วนงานการจัดการหนี้
    #ส่งเสริมสุขภาพและป้องกันโรค
    #เจ็บป่วยฉุกเฉินวิกฤต
    #ยาแฟคเตอร์
    #Take Over
    Click Link    ${module}

rowsOfSearchResult
    [Arguments]  ${rows}
    Wait Until Page Contains Element  xpath://table[2]/tbody/tr[2]/td
    Element Should Contain  xpath://table[2]/tbody/tr[2]/td  ข้อมูลทั้งหมด ${rows} รายการ

SearchNotFound
    Element Should Contain  id:sysMsg  ไม่พบข้อมูล
