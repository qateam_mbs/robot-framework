*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}  http://192.168.2.17:9080
${UAT}  https://uat.sso.go.th
${PRD}  https://wa1.sso.go.th
#Please set environment first
${URL}  ${MB}/emra/main.emra
${emraHome}  ${URL}?module=system&app=SYS0101&job=home
${EMRA01001}  ${URL}?module=emra&app=EMRA01001&job=init
${EMRA01002}  ${URL}?module=system&app=SYS0102&job=init
${EMRA01003}  ${URL}?module=emra&app=EMRA01003&job=init
${EMRA01004}  ${URL}?module=emra&app=EMRA01004&job=init
${EMRA01005}  ${URL}?module=emra&app=EMRA01005&job=init
${EMRA01006}  ${URL}?module=emra&app=EMRA01006&job=init
${EMRA01007}  ${URL}?module=emra&app=EMRA01007&job=init
${EMRA02003}  ${URL}?module=emra&app=EMRA02003&job=init
${EMRA02004}  ${URL}?module=emra&app=EMRA02004&job=init
${EMRA02005}  ${URL}?module=emra&app=EMRA02005&job=init
${EMRA02006}  ${URL}?module=emra&app=EMRA02006&job=init
${EMRA03001}  ${URL}?module=emra&app=EMRA03001&job=init
${EMRA03002}  ${URL}?module=emra&app=EMRA03002&job=init
${EMRA03003}  ${URL}?module=emra&app=EMRA03003&job=init
${EMRA03004}  ${URL}?module=emra&app=EMRA03004&job=init
${EMRA03005}  ${URL}?module=emra&app=EMRA03005&job=init
${EMRA03006}  ${URL}?module=emra&app=EMRA03006&job=init
${EMRA03007}  ${URL}?module=emra&app=EMRA03007&job=init
${EMRA03008}  ${URL}?module=emra&app=EMRA03008&job=init
${EMRA03009}  ${URL}?module=emra&app=EMRA03009&job=init
${EMRA03010}  ${URL}?module=emra&app=EMRA03010&job=init
${EMRA03011}  ${URL}?module=emra&app=EMRA03011&job=init
${EMRA03012}  ${URL}?module=emra&app=EMRA03012&job=init
${EMRA03013}  ${URL}?module=emra&app=EMRA03013&job=init
${EMRA03014}  ${URL}?module=emra&app=EMRA03014&job=init
${EMRA03015}  ${URL}?module=emra&app=EMRA03015&job=init
${EMRA03016}  ${URL}?module=emra&app=EMRA03016&job=init
${EMRA03017}  ${URL}?module=emra&app=EMRA03017&job=init
${EMRA04001}  ${URL}?module=emra&app=EMRA04001&job=init
${EMRA04002}  ${URL}?module=emra&app=EMRA04002&job=init
${EMRA05001}  ${URL}?module=emra&app=EMRA05001&job=init
${SYS0102}  ${URL}?module=system&app=SYS0102&job=init
${SYS0103}  ${URL}?module=system&app=SYS0103&job=init
${SYS0104}  ${URL}?module=system&app=SYS0104&job=init
${SYS0105}  ${URL}?module=system&app=SYS0105&job=init
${SYS0106}  ${URL}?module=system&app=SYS0106&job=init
${SYS0301}  ${URL}?module=system&app=SYS0301&job=init
${SYS0302}  ${URL}?module=system&app=SYS0302&job=init
${SYS0303}  ${URL}?module=system&app=SYS0303&job=init
${SYS0401}  ${URL}?module=system&app=SYS0401&job=init


*** Keywords ***
gotoProject
    [Arguments]  ${user}  ${pass}
    #Set Selenium Speed        0.5
    Open Browser    ${URL}  gc
    Maximize Browser Window

    #login
    Input Text    id:userName    ${user}
    Input Password    id:password    ${pass}
    Click Element    //button[@class="login-btn"]

rowsOfSearchResult
    [Arguments]  ${rows}
    Element Should Contain  xpath://table[2]/tbody/tr[2]/td  ข้อมูลทั้งหมด ${rows} รายการ

SearchNotFound
    Element Should Contain  xpath://*[@id="sysMsg"]/div/p  ไม่พบข้อมูล
