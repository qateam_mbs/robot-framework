*** Settings ***
Library  SeleniumLibrary

*** Variables ***

*** Keywords ***
openWeb
    [Arguments]  ${url}  ${browser}
    Open Browser    ${url}  ${browser}
    Maximize Browser Window

gotoHome
    Go To    ${Home}
