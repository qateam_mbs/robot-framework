*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}   http://sportsci.dpe.go.th/
#${UAT}
#${PRD}
${LoginPage}	${MB}login.php
${Home}	 ${MB}index.php
#${RMS000101}	${URL}rms00?app=RMS000101&job=init
#${RMS000102}	${URL}rms00?app=RMS000102&job=init

*** Keywords ***
gotoProject
    [Arguments]  ${user}  ${pass}
    #Set Selenium Speed        0.5
    Open Browser    ${LoginPage}   gc
    Maximize Browser Window

    Input Text    name:user  ${user}
    Input Password    name:pass    ${pass}
    Press Keys    name:pass  RETURN

Search result
    [Arguments]  ${text}
    Wait Until Page Contains Element  id:show-form
    Element Should Contain  id:show-form  ${text}
