*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}   http://192.168.2.19:8080
#${UAT}
#${PRD}
${LoginPage}	${MB}/ssosys/login
${URL}	        ${MB}/ssosys
${Home}         ${MB}/ssosys/home
${BEN01001}	    ${URL}/ben01001/*
${PAY06001}	    ${URL}/pay06001
${REPORT}	    ${URL}/report
${TESTSERV01}	${URL}/testserv/01/
${ben01002}	    ${URL}/ben01002/*
${ben02001}	    ${URL}/ben02001
${mas01001}	    ${URL}/mas01001/*
${mas01002}	    ${URL}/mas01002/*
${mas01003}	    ${URL}/mas01003/*
${mas01004}	    ${URL}/mas01004/*
${mas01005}	    ${URL}/mas01005/*
${sys01001}	    ${URL}/sys01001/*
${sys01002}	    ${URL}/sys01002/*
${sys01003}	    ${URL}/sys01003/*
${sys01004}	    ${URL}/sys01004/*
${sys01005}	    ${URL}/sys01005/*

*** Keywords ***
gotoProject&SelectModule
    [Arguments]  ${user}  ${pass}  ${module}
    #Set Selenium Speed        0.5
    Open Browser    ${LoginPage}   gc
    Maximize Browser Window

    Input Text    id:username  ${user}
    Input Password    id:password    ${pass}
    Press Keys    id:password  RETURN
    Loading Screen
    #Module
    #การจัดการ
    #ประโยชน์
    #การเงิน
    Click Element  xpath://label[contains(text(), "${module}")]
    Loading Screen

Loading Screen
	Wait Until Element Is Not Visible    //div[@id="system-overlay-loading-screen"]

rowsOfSearchResult
    [Arguments]  ${rows}
    Element Should Contain  xpath://div/div/span[2]  / ${rows} รายการ

SearchNotFound
    Element Should Contain  id:system-alert-message  ไม่พบข้อมูล
