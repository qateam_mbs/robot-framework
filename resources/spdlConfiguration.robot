*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}	http://192.168.2.16
${UAT}	http://172.20.90.88:9080
${PRD}	http://intranet.sso.go.th
${LoginPage}	${MB}/spdl/index.jsp?app=indexadmin
${URL}  ${MB}/spdl/
${Home}  ${URL}main.app
${ADM0101}	${URL}adm?app=ADM0101&job=init
${ADM0102}	${URL}adm?app=ADM0102&job=init
${ADM0103}	${URL}adm?app=ADM0103&job=init
${ADM0104}	${URL}adm?app=ADM0104&job=init
${ADM0105}	${URL}adm?app=ADM0105&job=init
${ADM0201}	${URL}adm?app=ADM0201&job=init
${ADM0202}	${URL}adm?app=ADM0202&job=init
${ADM0203}	${URL}adm?app=ADM0203&job=init
${ADM0301}	${URL}adm?app=ADM0301&job=init
${ADM0302}	${URL}adm?app=ADM0302&job=init
${ADM0303}	${URL}adm?app=ADM0303&job=init
${ADM0304}	${URL}adm?app=ADM0304&job=init
${ADM0305}	${URL}adm?app=ADM0305&job=init
${ADM0306}	${URL}adm?app=ADM0306&job=init
${ADM0307}	${URL}adm?app=ADM0307&job=init
${ADM0308}	${URL}adm?app=ADM0308&job=init
${ADM0309}	${URL}adm?app=ADM0309&job=init
${ADM0310}	${URL}adm?app=ADM0310&job=init
${ADM0311}	${URL}adm?app=ADM0311&job=init
${ADM0312}	${URL}adm?app=ADM0312&job=init
${ADM0313}	${URL}adm?app=ADM0313&job=init
${ADM0314}	${URL}adm?app=ADM0314&job=init
${ADM0315}	${URL}adm?app=ADM0315&job=init
${SPDL0101}	${URL}app?app=SPDL0101&job=init
${SPDL0102}	${URL}app?app=SPDL0102&job=init
${SPDL0103}	${URL}app?app=SPDL0103&job=init
${SPDL0104}	${URL}app?app=SPDL0104&job=init
${SPDL0105}	${URL}app?app=SPDL0105&job=init
${SPDL0106}	${URL}app?app=SPDL0106&job=init
${SPDL0201}	${URL}app?app=SPDL0201&job=init
${SPDL0202}	${URL}app?app=SPDL0202&job=init
${SPDL0301}	${URL}app?app=SPDL0301&job=init
${SPDL0302}	${URL}app?app=SPDL0302&job=init
${SPDL0303}	${URL}app?app=SPDL0303&job=init
${SPDL0401}	${URL}app?app=SPDL0401&job=init
${SPDL0402}	${URL}app?app=SPDL0402&job=init
${SPDL0403}	${URL}app?app=SPDL0403&job=init
${SPDL0404}	${URL}app?app=SPDL0404&job=init
${SPDL0501}	${URL}app?app=SPDL0501&job=init
${SPDL0502}	${URL}app?app=SPDL0502&job=init
${SPDL0503}	${URL}app?app=SPDL0503&job=init
${SPDL0601}	${URL}app?app=SPDL0601&job=init
${SPDL0602}	${URL}app?app=SPDL0602&job=init
${SPDL0603}	${URL}app?app=SPDL0603&job=init
${SPDL0604}	${URL}app?app=SPDL0604&job=init
${SPDL0701}	${URL}app?app=SPDL0701&job=init
${SPDL0702}	${URL}app?app=SPDL0702&job=init
${SPDL0703}	${URL}app?app=SPDL0703&job=init
${SPDL0704}	${URL}app?app=SPDL0704&job=init
${SPDL0705}	${URL}app?app=SPDL0705&job=init
${SPDL0706}	${URL}app?app=SPDL0706&job=init
${SPDL0707}	${URL}app?app=SPDL0707&job=init
${SPDL0708}	${URL}app?app=SPDL0708&job=init
${SPDL0709}	${URL}app?app=SPDL0709&job=init
${SPDL0710}	${URL}app?app=SPDL0710&job=init
${SPDL0801}	${URL}app?app=SPDL0801&job=init
${SPDL0802}	${URL}app?app=SPDL0802&job=init
${SPDL0901}	${URL}app?app=SPDL0901&job=init
${SPDL0902}	${URL}app?app=SPDL0902&job=init
${SPDL0903}	${URL}app?app=SPDL0903&job=init
${SPDL0904}	${URL}app?app=SPDL0904&job=init
${SPDL1001}	${URL}app?app=SPDL1001&job=init
${SPDL1002}	${URL}app?app=SPDL1002&job=init
${SPDL1003}	${URL}app?app=RPTCENTER&screencode=SPDLR01038&job=init&url=jsp/rpt/SPDLR01038.jsp

*** Keywords ***
gotoProject&SelectModule
    [Arguments]  ${user}  ${module}
    Set Selenium Speed        0.5
    Open Browser    ${LoginPage}   gc
    Maximize Browser Window

    Input Text    name:userid  ${user}
    Press Keys    name:userid  RETURN
    #ตั้งค่าระบบใช้ setup, ระบบบำนาญชราภาพใช้ spd
    Wait Until Page Contains Element   //button[@class="${module}-module"]
    Click Element  //button[@class="${module}-module"]

rowsOfSearchResult
    [Arguments]  ${rows}
    Element Should Contain  xpath://table[3]/tbody/tr/td[1]  ข้อมูลทั้งหมด ${rows} รายการ

SearchNotFound
    Element Should Contain  id:msg  ไม่พบข้อมูลที่ต้องการค้นหา กรุณาตรวจสอบเงื่อนไขที่ระบุ

saveSuccess
    Alert Should Be Present  กรุณายืนยันการบันทึกข้อมูล  ACCEPT
    Element Should Contain  id:msg  ระบบบันทึกข้อมูลเรียบร้อยแล้ว
