*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}  http://192.168.2.19:8080
#${UAT}  https://uat.sso.go.th
#${PRD}  https://wa1.sso.go.th
#Please set environment first
${URL}  ${MB}/ssosys/
${edentHome}   ${URL}home       #หน้าแรก Index
${EDENT01001}  ${URL}ben01001   #บันทึกรับแจ้ง กณีทันตกรรม
${EDENT01002}  ${URL}ben01002   #ค้นหา/แก้ไขเรื่องรับแจ้งขอรับสิทธิประโยชน์ กรณีทันตกรรม
${EDENT02001}  ${URL}ben02001   #บันทึกผลการวินิจฉัย กรณีทันตกรรม
${EDENT02002}  ${URL}ben02002   #แก้ไข/ยกเลิกผลการวินิจฉัย กรณีทันตกรรม
${EDENT03001}  ${URL}ben03001   #สร้างคำสั่งจ่าย กรณีทันตกรรม
${EDENT03002}  ${URL}ben03002   #แก้ไข/ยกเลิกคำสั่งจ่าย กรณีทันตกรรม
${edentReport}  ${URL}report    #รายงาน

*** Keywords ***
gotoProject
    [Arguments]  ${user}  ${pass}
    #Set Selenium Speed        0.5
    Open Browser    ${URL}  gc
    Maximize Browser Window

    #login
    Input Text    id:userName    ${user}
    Input Password    id:password    ${pass}
    Click Element    //button[@type="submit"]
