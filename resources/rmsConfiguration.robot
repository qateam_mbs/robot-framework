*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${MB}	http://192.168.2.15:9080
#${UAT}
#${PRD}
${LoginPage}	${MB}/rms/
${URL}	 ${MB}/rms/
${Home}	 ${URL}
${RMS000101}	${URL}rms00?app=RMS000101&job=init
${RMS000102}	${URL}rms00?app=RMS000102&job=init
${RMS000103}	${URL}rms00?app=RMS000103&job=init
${RMS000104}	${URL}rms00?app=RMS000104&job=init
${RMS000201}	${URL}rms00?app=RMS000201&job=init
${RMS000202}	${URL}rms00?app=RMS000202&job=init
${RMS000203}	${URL}rms00?app=RMS000203&job=init
${RMS000301}	${URL}rms00?app=RMS000301&job=init
${RMS010101}	${URL}rms01?app=RMS010101&job=init
${RMS010102}	${URL}rms01?app=RMS010102&job=init
${RMS010201}	${URL}rms01?app=RMS010201&job=init
${RMS010202}	${URL}rms01?app=RMS010202&job=init
${RMS010301}	${URL}rms01?app=RMS010301&job=init
${RMS010302}	${URL}rms01?app=RMS010302&job=init
${RMS010303}	${URL}rms01?app=RMS010303&job=init
${RMS010401}	${URL}rms01?app=RMS010401&job=init
${RMS010402}	${URL}rms01?app=RMS010402&job=init
${RMS010403}	${URL}rms01?app=RMS010403&job=init
${RMS010404}	${URL}rms01?app=RMS010404&job=init
${RMS010405}	${URL}rms01?app=RMS010405&job=init
${RMS010406}	${URL}rms01?app=RMS010406&job=init
${RMS010407}	${URL}rms01?app=RMS010407&job=init
${RMS010501}	${URL}rms01?app=RMS010501&job=init
${RMS010502}	${URL}rms01?app=RMS010502&job=init
${RMS010503}	${URL}rms01?app=RMS010503&job=init
${RMS010504}	${URL}rms01?app=RMS010504&job=init
${RMS020101}	${URL}rms02?app=RMS020101&job=init
${RMS020102}	${URL}rms02?app=RMS020102&job=init
${RMS020103}	${URL}rms02?app=RMS020103&job=init
${RMS020201}	${URL}rms02?app=RMS020201&job=init
${RMS020202}	${URL}rms02?app=RMS020202&job=init
${RMS020301}	${URL}rms02?app=RMS020301&job=init
${RMS020302}	${URL}rms02?app=RMS020302&job=init
${RMS020303}	${URL}rms02?app=RMS020303&job=init
${RMS020304}	${URL}rms02?app=RMS020304&job=init
${RMS020305}	${URL}rms02?app=RMS020305&job=init
${RMS020401}	${URL}rms02?app=RMS020401&job=init
${RMS020402}	${URL}rms02?app=RMS020402&job=init
${RMS020403}	${URL}rms02?app=RMS020403&job=init
${RMS020404}	${URL}rms02?app=RMS020404&job=init
${RMS020405}	${URL}rms02?app=RMS020405&job=init
${RMS020406}	${URL}rms02?app=RMS020406&job=init
${RMS020407}	${URL}rms02?app=RMS020407&job=init
${RMS020501}	${URL}rms02?app=RMS020501&job=init
${RMS020502}	${URL}rms02?app=RMS020502&job=init
${RMS020503}	${URL}rms02?app=RMS020503&job=init
${RMS020504}	${URL}rms02?app=RMS020504&job=init
${RMS030101}	${URL}rms03?app=RMS030101&job=init
${RMS030102}	${URL}rms03?app=RMS030102&job=init
${RMS030103}	${URL}rms03?app=RMS030103&job=init
${RMS030201}	${URL}rms03?app=RMS030201&job=init
${RMS030202}	${URL}rms03?app=RMS030202&job=init
${RMS030301}	${URL}rms03?app=RMS030301&job=init
${RMS030302}	${URL}rms03?app=RMS030302&job=init
${RMS030303}	${URL}rms03?app=RMS030303&job=init
${RMS030401}	${URL}rms03?app=RMS030401&job=init
${RMS030402}	${URL}rms03?app=RMS030402&job=init
${RMS030403}	${URL}rms03?app=RMS030403&job=init
${RMS030404}	${URL}rms03?app=RMS030404&job=init
${RMS030405}	${URL}rms03?app=RMS030405&job=init
${RMS030406}	${URL}rms03?app=RMS030406&job=init
${RMS030407}	${URL}rms03?app=RMS030407&job=init
${RMS030501}	${URL}rms03?app=RMS030501&job=init
${RMS030502}	${URL}rms03?app=RMS030502&job=init
${RMS030503}	${URL}rms03?app=RMS030503&job=init
${RMS030504}	${URL}rms03?app=RMS030504&job=init

*** Keywords ***
gotoProject&SelectModule
    [Arguments]  ${user}  ${pass}  ${module}
    #Set Selenium Speed        0.5
    Open Browser    ${LoginPage}   gc
    Maximize Browser Window

    Input Text    name:uid  ${user}
    Input Password    name:password    ${pass}
    Press Keys    name:password  RETURN
    #Module
    #ตั้งค่าระบบ
    #แพทย์ผู้เชี่ยวชาญ
    #ทุพพลภาพ(IPD)
    #ระบบไตเทียม
    Click Button  xpath: //button[contains(text(), "${module}")]

rowsOfSearchResult
    [Arguments]  ${rows}
    Element Should Contain  xpath://table[3]/tbody/tr/td[1]  ข้อมูลทั้งหมด ${rows} รายการ

SearchNotFound
    Element Should Contain  id:msg  ไม่พบข้อมูลที่ต้องการค้นหา กรุณาตรวจสอบเงื่อนไขที่ระบุ
