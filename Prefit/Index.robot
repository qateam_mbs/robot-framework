*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/preFitConfiguration.robot
Suite Setup     gotoProject  admin  passw0rd
Suite Teardown  Close Browser
Test Setup  Go To  ${Home}

*** Variables ***


*** Test Cases ***
index_Nor_Anyone_Search_on Condition_Found
	[Tags]  Unit  UAT  Flow
	Input Condition for Search   2019  04
	Search result  ทดสอบสมรรถภาพทางกาย ณ โรงเรียนเซนต์คาเบรียล

index_Neg_Anyone_Search_on Condition_Not Found
	Input Condition for Search   2019  12
	Search result	ไม่พบกิจกรรม

*** Keywords ***
Input Condition for Search
    [Arguments]  ${s_year}  ${s_month}
	Wait Until Page Contains Element    name:s_year
	Select From List By Value    name:s_year    ${s_year}
	Wait Until Page Contains Element    name:s_month
	Select From List By Value    name:s_month    ${s_month}
	Wait Until Page Contains Element    xpath://button[@class="btn btn-primary"]
	Click Element    //button[@class="btn btn-primary"]
