*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdlConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spdl
Suite Teardown  Close Browser
Test Setup  Go To  ${SPDL0106}
Test Template  saveToSpecialCase

*** Test Cases ***
#--------------- |---------------
#   Test Case    |  citizenId
#
#--------------------------------
1) 1409900185140   1409900185140
# 2) ม.33 A Test2   3100503544071

*** Keywords ***
saveToSpecialCase
    [Arguments]  ${citizenId}
    Input Text    id:citizenId    ${citizenId}
    Click Element  id:search
    Select Checkbox  id:isAllow
    clickSave

clickSave
    Click Element  id:save
    saveSuccess
