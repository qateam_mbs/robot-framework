*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdlConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spdl
Suite Teardown  Close Browser
Test Setup  Go To  ${SPDL0201}
Test Template  load data

*** Test Cases ***
#--------------- |---------------|-----------------
#   Test Case    |  citizenId    | deathDate
#
#--------------------------------------------------
1) ม.33 A Test1   1109306628452  10/05/2562
2) ม.33 A Test2   3100503544071  08/05/2562

*** Keywords ***
load data
    [Arguments]  ${citizenId}  ${deathDate}
    Input Text    id:citizenId    ${citizenId}
    Click Element  id:view
    canLoadDetail
    addDeathdate  ${deathDate}

addDeathdate
    [Arguments]  ${deathDate}
    Click Element  id:edit
    Input Text    id:deathDate    ${deathDate}
    Input Text    id:password     password
    Click Element  id:save
    saveSuccess

canLoadDetail
    Wait Until Page Contains Element  xpath://div[contains(text(), "SPDL0201B")]
