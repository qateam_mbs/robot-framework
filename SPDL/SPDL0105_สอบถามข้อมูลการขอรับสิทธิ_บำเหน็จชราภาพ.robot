*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdlConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spdl
#Suite Teardown  Close Browser
Test Setup  Go To  ${SPDL0105}

*** Test Cases ***
SPDL0105A_Nor_BEN_Search_Data on Condition_Found
    [Tags]  Unit  UAT
    Input Condition for Search  5360500043246  ${EMPTY}
    rowsOfSearchResult  1

SPDL0105A_Neg_BEN_Search_Data on Condition_Not Found
    Input Condition for Search  5360500043242  ${EMPTY}
    SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${citizenId}  ${pettitionNo}
    Input Text    id:citizenId    ${citizenId}
    Input Text    id:pettitionNo  ${pettitionNo}
    Click Element  id:search
