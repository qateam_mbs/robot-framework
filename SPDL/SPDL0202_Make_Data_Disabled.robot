*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdlConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spdl
Suite Teardown  Close Browser
Test Setup  Go To  ${SPDL0202}
Test Template  load data

*** Test Cases ***
#--------------- |---------------|-----------------
#   Test Case    |  citizenId    | disabledDate   |  committeeDate  |  useResignDate
#
#--------------------------------------------------
1) ม.33 A Test1   1109306628452  10/05/2562  12/05/2562  N
2) ม.33 A Test2   3100503544071  08/05/2562  02/05/2562  Y

*** Keywords ***
load data
    [Arguments]  ${citizenId}  ${disabledDate}  ${committeeDate}  ${useResignDate}
    Input Text    id:citizenId    ${citizenId}
    Click Element  id:view
    canLoadDetail
    addDisabledDate  ${disabledDate}  ${committeeDate}  ${useResignDate}

addDisabledDate
    [Arguments]  ${disabledDate}  ${committeeDate}  ${useResignDate}
    Click Element  id:edit
    Input Text    id:disabledDate    ${disabledDate}
    Input Text    id:committeeDate    ${committeeDate}
    Run Keyword If    '${useResignDate}'=='Y'  Select Checkbox  id:useResignDate
    Input Text    id:password     password
    Click Element  id:save
    saveSuccess

canLoadDetail
    Wait Until Page Contains Element  xpath://div[contains(text(), "SPDL0202B")]
