*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdlConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spdl
#Suite Teardown  Close Browser
Test Setup  Go To  ${SPDL0103}
Test Template  load data for save

*** Test Cases ***
#---------------|---------|-----------------
#   Test Case   |  citizenId  | ReceiveBy(B,D,O,P) | BankCode| Account | postOffice | specialType | lastBenAmount | calcBenYear |payStart | payEnd  | entitledDate
# ReceiveBy:B | BankCode
# 002 : ธนาคารกรุงเทพ จำกัด (มหาชน)
# 004 : ธนาคารกสิกรไทย จำกัด (มหาชน)
# 006 : ธนาคารกรุงไทย จำกัด (มหาชน)
# 011 : ธนาคารทหารไทย จำกัด (มหาชน)
# 014 : ธนาคารไทยพาณิชย์ จำกัด (มหาชน)
# 022 : ธนาคารซีไอเอ็มบี ไทย จำกัด (มหาชน)
# 025 : ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)
# 065 : ธนาคารธนชาต จำกัด (มหาชน)
# 066 : ธนาคารอิสลามแห่งประเทศไทย
#-------------------------------------------
# ReceiveBy:D | BankCode| Account | ${EMPTY} | 1
# 030 : ธนาคารออมสิน
# 034 : ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร
#-------------------------------------------
# ReceiveBy:O | ${EMPTY}| ${EMPTY} | ${EMPTY}| 2
# ReceiveBy:P | ${EMPTY}| ${EMPTY} | postOffice | 3
# --------specialType--------
# 1  ผลประโยชน์ตกค้าง
# 2  เงินสมทบพร้อมผลประโยชน์ตกค้าง
# 3  คำนวนจ่ายเฉพาะเงินสมทบฝ่ายลูกจ้าง
#-------------------------------------------
# lastBenAmount   calcBenYear
# 25000           2561
#-------------------------------------------
# payStart    payEnd      entitledDate
# 2550/01     2561/05     30/05/2561
#-------------------------------------------
1) 1409900185140 B   1409900185140  B  004  0272182169  ${EMPTY}  1  25000  2561  ${EMPTY}  ${EMPTY}  ${EMPTY}
2) 1409900185140 D   1409900185140  D  030  123456789012  ${EMPTY}  2  ${EMPTY}  ${EMPTY}  2550/01  2561/05  30/05/2561
3) 1409900185140 O   1409900185140  O  ${EMPTY}  ${EMPTY}  ${EMPTY}  3  ${EMPTY}  ${EMPTY}  2550/01  2561/05  30/05/2561

*** Keywords ***
load data for save
    [Arguments]  ${citizenId}  ${ReceiveBy}  ${BankCode}  ${Account}  ${postOffice}  ${specialType}  ${lastBenAmount}  ${calcBenYear}   ${payStart}   ${payEnd}   ${entitledDate}
    Input Text    id:citizenId    ${citizenId}
    Click Element  id:loadData
    addAddress
    inputReceiveBy    ${ReceiveBy}  ${BankCode}  ${Account}  ${postOffice}
    checkRadioDocCompleted
    Select Radio Button    specialType    ${specialType}
    Run Keyword If    ${specialType}==1    specialType1  ${lastBenAmount}  ${calcBenYear}
    ...  ELSE  specialType23  ${payStart}   ${payEnd}   ${entitledDate}
    clickSave

canLoadDetail
    Wait Until Page Contains Element  xpath://div[contains(text(), "SPDL0103B")]

addAddress
    canLoadDetail

    Wait Until Page Contains Element    id:homeId
    Clear Element Text  id:homeId
    Input Text    id:homeId   111/22

    Wait Until Page Contains Element    id:provinceName
    Clear Element Text  id:provinceName
    delayAjax    provinceName    นนทบุรี  N

    Wait Until Page Contains Element    id:amphurName
    Clear Element Text  id:amphurName
    delayAjax    amphurName    อ.เมืองนนทบุรี  N

    Wait Until Page Contains Element    id:tambolName
    Clear Element Text  id:tambolName
    delayAjax    tambolName    บางเขน  N

    Wait Until Page Contains Element    id:postCode
    Clear Element Text  id:postCode
    Input Text    id:postCode  11000

delayAjax
    [Arguments]   ${locatorID}  ${value}  ${isDoubleDown}
    Set Focus To Element  id:${locatorID}
    Set Selenium Speed        0.5
    Input Text    id:${locatorID}  ${value}
    Sleep    1s
    Run Keyword If  '${isDoubleDown}'=='N'
    ...  Press Keys  None  ARROW_DOWN  RETURN
    ...  ELSE  Press Keys  None  ARROW_DOWN  ARROW_DOWN  RETURN
    Set Selenium Speed        0.5

checkRadioDocCompleted
    Select Radio Button    isDocCompleted    Y

inputReceiveBy
    [Arguments]   ${ReceiveBy}  ${BankCode}  ${Account}  ${postOffice}
    Wait Until Page Contains Element  id:entitledMethod
    Set Focus To Element  id:entitledMethod
    Select From List By Value    id:entitledMethod    ${ReceiveBy}
    Log To Console    \nReceiveBy:${ReceiveBy}

    Run Keyword If  '${ReceiveBy}'=='B'  inputBank&Acc    ${BankCode}  ${Account}
    ...  ELSE IF  '${ReceiveBy}'=='D'  inputBank&Acc      ${BankCode}  ${Account}
    ...  ELSE IF  '${ReceiveBy}'=='P'  inputPostOffice  ${postOffice}

inputBank&Acc
    [Arguments]   ${BankCode}  ${Account}
    Wait Until Page Contains Element    id:bankName
    delayAjax    bankName    ${BankCode}    Y
    Wait Until Page Contains Element    id:bankAccNo
    Input Text    id:bankAccNo    ${Account}

inputPostOffice
    [Arguments]   ${postOffice}
    Wait Until Page Contains Element    name:postOffice
    Input Text    name:postOffice    ${postOffice}

clickSave
    Click Element    id:save
    saveSuccess

specialType1
    [Arguments]   ${lastBenAmount}  ${calcBenYear}
    Input Text    id:lastBenAmount    ${lastBenAmount}
    Input Text    id:calcBenYear      ${calcBenYear}

specialType23
    [Arguments]   ${payStart}  ${payEnd}  ${entitledDate}
    Input Text    id:payStart   ${payStart}
    Input Text    id:payEnd     ${payEnd}
    Click Element    id:loadCntr
    Wait Until Element Is Not Visible   class:sticky-overlay
    Input Text    id:entitledDate    ${entitledDate}
