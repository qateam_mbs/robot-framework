*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdlConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spdl
Suite Teardown  Close Browser
Test Setup  Go To  ${SPDL0301}
Test Template  load data for cal

*** Test Cases ***
#---------------|---------|-----------------
#   Test Case   |  citizenId  | pettitionNo | createdDate| ssoBranchCode
#
#-------------------------------------------
1) ม.33 A   1109306628452  ${EMPTY}  ${EMPTY}  1000
2) ม.33 A   3100503544071  ${EMPTY}  ${EMPTY}  1000

*** Keywords ***
load data for cal
    [Arguments]  ${citizenId}  ${pettitionNo}  ${createdDate}  ${ssoBranchCode}
    Search on Condition  ${citizenId}  ${pettitionNo}  ${createdDate}  ${ssoBranchCode}
    View Detail    ${citizenId}    2    8
    inputpass&submit

Search on Condition
    [Arguments]  ${citizenId}  ${pettitionNo}  ${createdDate}  ${ssoBranchCode}
    Input Text    id:citizenId    ${citizenId}
    Input Text    id:pettitionNo    ${pettitionNo}
    Input Text    id:createdDate    ${createdDate}
    Select From List By Value    id:ssoBranchCode    ${ssoBranchCode}
    Click Element  id:search

View Detail
    [Arguments]  ${value}  ${searchColumn}  ${clickColumn}
    ${rows} =    Get Element Count   xpath=//*[@id="searchForm"]/table[2]/tbody/tr
    :FOR    ${rowindex}    IN RANGE    1    ${rows + 1}
    \    ${result} =  Run Keyword And Return Status    Element Text Should Be  xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[${searchColumn}]  ${value}
    \    ${foundRow}=  Set Variable If    ${result}    ${rowindex}
    \    Run Keyword If    ${result}    Exit For Loop
    Click Element    xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[${clickColumn}]/a

inputpass&submit
    canLoadDetail
    Input Text    id:password    password
    Click Element    id:save
    Alert Should Be Present  กรุณายืนยันการบันทึกข้อมูล  ACCEPT
    Wait Until Element Is Not Visible   class:sticky-overlay
    Element Should Contain  id:msg  สร้างรหัสการคำนวณเงินเสร็จสิ้น

canLoadDetail
    Wait Until Page Contains Element  xpath://div[contains(text(), "SPDL0301B")]
