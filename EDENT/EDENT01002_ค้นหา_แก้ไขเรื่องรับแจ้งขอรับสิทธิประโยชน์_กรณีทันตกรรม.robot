*** Setting ***
Library  SeleniumLibrary
Resource  ../resources/edentConfiguration.robot
Suite Setup     gotoProject  admin  password
#Suite Teardown  Close Browser
Test Setup  Go To  ${EDENT01002}

*** Variables ***

*** Test Cases ***
EDENT01002 Normal Search Input Data  
    [Tags]  Value1
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ
    Input Condition for Search  100062000018676  3471200430639  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr/td[7]  A : อนุมัติแล้ว

EDENT01002 Normal Search Input Data but none petitionNo   
    [Tags]  Value1.1
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล Petition_No
    Input Condition for Search  ${Empty}  3471200430639  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr/td[7]  A : อนุมัติแล้ว

EDENT01002 Normal Search Input Data but none citizenId 
    [Tags]  Value1.2
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล CitizenId
    Input Condition for Search  100062000018676  ${Empty}  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr/td[7]  A : อนุมัติแล้ว

EDENT01002 Normal Search Input Condition with no data found  
    [Tags]  Value2
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ กรณีไม่พบข้อมูล
    Input Condition for Search  123165485118416  3212145481231  22/06/2562  24/06/2562  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT01002 Negative Search No Data Input Found 
    [Tags]  Value3
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกข้อมูล
    Input Condition for Search  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#ssoBranchCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#benefitCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT01002 Negative Search No Some Data Input Found 
    [Tags]  Value4
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล ประเภทสิทธิฯ, กรณีรักษา, สิทะิประโยชน์ 
    Input Condition for Search  ${Empty}  3212145481231  22/06/2562  ${Empty}  1000  ${Empty}  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#benefitCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT01002 Negative Search No Some Data Input Found 
    [Tags]  Value4.1
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล กรณีรักษา, สิทะิประโยชน์ 
    Input Condition for Search  ${Empty}  3212145481231  22/06/2562  ${Empty}  1000  1  ${Empty}  ${Empty}
    #Element Text Should Be  //*[@id="#benefitCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT01002 Negative Search Without Complete Data PetitionNo
    [Tags]  Value5
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกข้อมูล PetitionNo ไม่ครบ
    Input Condition for Search  1000620000186  3471200430639  ${Empty}  ${Empty}  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT01002 Negative Search Without Complete Data CitizenId    #ทดสอบการค้นหาข้อมูลโดยกรอกข้อมูลไม่ครบ
    [Tags]  Value5.1
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล CitizenId ไม่ครบ
    Input Condition for Search  100062000018676  347120043  ${Empty}  ${Empty}  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล


*** Keywords ***
Input Condition for Search
    [Arguments]  ${petitionNo}  ${citizenId}  ${claimDate}  ${petitionDate}  ${ssoBranchCode}  ${benefitCode}  ${claimCaseCode}  ${claimBenefitCode}
    Wait Until Page Contains Element  id:petitionNo
    Input Text  id:petitionNo  ${petitionNo}

    Wait Until Page Contains Element  id:citizenId
    Input Text  id:citizenId  ${citizenId}

    Wait Until Page Contains Element  id:claimDate
    Input Text  id:claimDate  ${claimDate}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:petitionDate
    Input Text  id:petitionDate  ${petitionDate}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:ssoBranchCode 
    Click Element    //span[@class="filter-option pull-left"]
    Select From List By Value    //select[@name="petition.ssoBranchCode"]  ${ssoBranchCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:benefitCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[2]
    Select From List By Value    //select[@name="petition.benefitCode"]  ${benefitCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimCaseCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[3]
    Select From List By Value    //select[@name="petitionDetail.claimCaseCode"]  ${claimCaseCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimBenefitCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[4]
    Select From List By Value    //select[@name="petitionDetail.claimBenefitCode"]  ${claimBenefitCode}
    Press Keys    None    ESC

    Click Element  id:search
