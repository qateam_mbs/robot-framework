*** Setting ***
Library  SeleniumLibrary
Resource  ../resources/edentConfiguration.robot
Suite Setup     gotoProject  admin  password
#Suite Teardown  Close Browser
Test Setup  Go To  ${EDENT01001}

*** Variables ***

*** Test Cases ***
EDENT01002 Normal Search Input Data 
    [Tags]  Value1
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ
    Input Condition for Search  3471200430639  01/07/2562  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div/div/div[1]/div[1]  รายละเอียดผู้ประกันตน
    Click Link  xpath=(//a[@href="http://192.168.2.19:8080/ssosys/ben01001/"])

EDENT01002 Normal Search Input Condition with no data found 
    [Tags]  Value2
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ กรณีไม่พบข้อมูล
    Input Condition for Search  3100502626201  22/06/2562  1  T  11
    Element Text Should Not Be  //*[@id="system-alert-message"]  BEN01001_W001 : ไม่พบข้อมูลผู้ประกันตน

EDENT01002 Negative Search Input none data with Citizen Id
    [Tags]  Value3
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่กรอกเลขที่บัตรประชาชน
    Input Condition for Search  ${empty}  22/06/2562  1  T  11
    Element Text Should Be  //*[@id="#citizenId"]  โปรดระบุเลขบัตรประชาชน

EDENT01002 Negative Search Input Without Complete data Citizen Id
    [Tags]  Value3.1
    Comment  ทดสอบการค้นหาข้อมูลแบบกรอกเลขที่บัตรประชาชนไม่ครบ 13 หลัก
    Input Condition for Search  3471200430  22/06/2562  1  T  11
    Element Text Should Be  //*[@id="#citizenId"]  จำนวนอักษร 13 หลักเท่านั้น

EDENT01002 Negative Search Input none data with TreatmentDate
    [Tags]  Value4
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่กรอกวันที่ใช้สิทธิ
    Input Condition for Search  3471200430639  ${empty}  1  T  11
    Element Text Should Be  //*[@id="#treatmentDate"]  โปรดระบุวันที่

EDENT01002 Negative Search Input Wrong data with TreatmentDate
    [Tags]  Value4.1
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่กรอกวันที่ใช้สิทธิ
    Input Condition for Search  3471200430639  01/07/269  1  T  11
    Element Text Should Be  //*[@id="#treatmentDate"]  โปรดระบุวันที่

EDENT01002 Normal Input Data Benefit Case I With Bank Payment
    [Tags]  Value5
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรม
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    #สร้างที่อยู่ผู้ประกันตน
    Input Address of Benefit Owner for submit  199/27  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #สร้างข้อมูลผู้ขอรับสิทธิ เคส I:ผู้ประกันตน
    Click Element    //label[@for="requester-type-I-entitled-new"]
    Input Benefit Owner for Payment case:I  B  002  234598778166258
    #สร้างรายการรักษาที่เบิก
    Input Benefit Payment Descriptions  112000000  1  900
 
    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Not Be  //*[@id="system-alert-message"]  I002 : บันทึกข้อมูลสำเร็จ

EDENT01002 Normal Input Data Benefit Case I With Bank Transfer
    [Tags]  Value5.1
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรม
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    #สร้างที่อยู่ผู้ประกันตน
    Input Address of Benefit Owner for submit  199/27  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #สร้างข้อมูลผู้ขอรับสิทธิ เคส I:ผู้ประกันตน
    Click Element    //label[@for="requester-type-I-entitled-new"]
    Input Benefit Owner for Payment case:I  D  014  234598778166258
    #สร้างรายการรักษาที่เบิก
    Input Benefit Payment Descriptions  112000000  1  900

    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Not Be  //*[@id="system-alert-message"]  I002 : บันทึกข้อมูลสำเร็จ

EDENT01002 Normal Input Data Benefit Case H With Hospital (ยังไม่สามารถทดสอบได้ เพราะยังไม่มีข้อมูล ร.พ.)
    [Tags]  Value5.2
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรม
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    #สร้างที่อยู่ผู้ประกันตน
    Input Address of Benefit Owner for submit  199/27  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #สร้างข้อมูลผู้ขอรับสิทธิ เคส I:ผู้ประกันตน
    Click Element    //label[@for="requesterTypeH"]
    Click Element    xpath=(//span[@class="filter-option pull-left"])[6]
    Select From List By Value    //select[@id="requester-type-H-entitled-new-citizenId"]    5
    #สร้างรายการรักษาที่เบิก
    Select From List By Value    //select[@id="treatmentCode-1"]    112000000
    Input Text    //input[@id="noOfUnit-1"]    1
    Input Text    //input[@id="claimAmount-1"]    900
    Press Keys    None    TAB
    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Not Be  //*[@id="system-alert-message"]  I002 : บันทึกข้อมูลสำเร็จ

EDENT01002 Normal Input Data Benefit Case H With Hospital but data found result
    [Tags]  Value5.3
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรม
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    #สร้างที่อยู่ผู้ประกันตน
    Input Address of Benefit Owner for submit  199/27  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #สร้างข้อมูลผู้ขอรับสิทธิ เคส I:ผู้ประกันตน
    Click Element    //label[@for="requesterTypeH"]
    Click Element    xpath=(//span[@class="filter-option pull-left"])[6]
    Select From List By Value    //select[@id="requester-type-H-entitled-new-citizenId"]    5
    #สร้างรายการรักษาที่เบิก
    Select From List By Value    //select[@id="treatmentCode-1"]    112000000
    Input Text    //input[@id="noOfUnit-1"]    1
    Input Text    //input[@id="claimAmount-1"]    900
    Press Keys    None    TAB
    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Not Be  //*[@id="system-alert-message"]  BEN01001_E012 : ไม่สามารถบันทึกข้อมูลรับแจ้งได้ เนื่องจากไม่พบข้อมูลวิธีการรับสิทธิ ของโรงพยาบาล

EDENT01002 Negative Input Data Benefit Without Complete (Address)
    [Tags]  Value6
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรมแบบกรอกข้อมูลไม่สมบูรณ์    #ไม่กรอกข้อมูลที่อยู่ไม่ครบ
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    Input Address of Benefit Owner for submit  ${empty}  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  ${empty}  ${empty}  ${empty}  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #ข้อมูลผู้ขอรับสิทธิ
    Click Element    //label[@for="requester-type-I-entitled-new"]
    Click Element    xpath=(//span[@class="filter-option pull-left"])[4]
    Select From List By Value    //select[@id="requester-type-I-entitled-new-entitledMethod"]    D
    Click Element    xpath=(//span[@class="filter-option pull-left"])[5]
    Select From List By Value    //select[@id="requester-type-I-entitled-new-bankCode"]    014
    Input Text    //input[@id="requester-type-I-entitled-new-bankAccNo"]    123665484999788
    #รายการรักษาที่เบิก
    Select From List By Value    //select[@id="treatmentCode-1"]    112000000
    Input Text    //input[@id="noOfUnit-1"]    1
    Input Text    //input[@id="claimAmount-1"]    900
    Press Keys    None    TAB
    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Be  //*[@id="#homeId"]  โปรดระบุเลขที่บ้าน
    Element Text Should Be  //*[@id="#provinceCode"]  โปรดระบุ

EDENT01002 Negative Input Data Benefit Case I Without Complete 1 (Benefit)
    [Tags]  Value6.1
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรมแบบกรอกข้อมูลไม่สมบูรณ์    #ไม่กรอกข้อมูลผู้ขอรับสิทธิไม่ครบ
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    Input Address of Benefit Owner for submit  23/5  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #ข้อมูลผู้ขอรับสิทธิ
    Click Element    //label[@for="requester-type-I-entitled-new"]
    Input Benefit Owner for Payment case:I  D  014  ${empty}
    #รายการรักษาที่เบิก
    Input Benefit Payment Descriptions  112000000  1  900

    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Be  //*[@id="requester-type-I"]/div/div[2]/div[2]/div/div[4]/div[2]/div[2]/div  โปรดระบุ

EDENT01002 Negative Input Data Benefit Case I Without Complete 2 (Benefit)
    [Tags]  Value6.2
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรมแบบกรอกข้อมูลไม่สมบูรณ์    #กรอกข้อมูลรายการรักษาที่เบิกไม่ครบ
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    Input Address of Benefit Owner for submit  23/5  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #ข้อมูลผู้ขอรับสิทธิ
    Click Element    //label[@for="requester-type-I-entitled-new"]
    Input Benefit Owner for Payment case:I  D  014  234598778166258
    #รายการรักษาที่เบิก
    Input Benefit Payment Descriptions  ${empty}  ${empty}  ${empty}

    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Be  //*[@id="#treatmentCode-1"]  โปรดระบุ

EDENT01002 Negative Input Data Benefit Case I Without Complete 3 (Benefit)
    [Tags]  Value6.3
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรมแบบกรอกข้อมูลไม่สมบูรณ์    #กรอกยอดเงินเกิน จำนวนที่เบิกจ่ายได้ 900 บาท
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    Input Address of Benefit Owner for submit  23/5  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #ข้อมูลผู้ขอรับสิทธิ
    Click Element    //label[@for="requester-type-I-entitled-new"]
    Input Benefit Owner for Payment case:I  D  014  234598778166258
    #รายการรักษาที่เบิก
    Input Benefit Payment Descriptions  112000000  ${empty}  1200

    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Be  //*[@id="#claimAmount-1"]  จำนวนต้องน้อยกว่าหรือเท่ากับ 900

EDENT01002 Negative Input Data Benefit Case H Without Complete (Benefit)
    [Tags]  Value6.4
    Comment  ทดสอบการบันทึกข้อมูลรับแจ้ง กรณีทันตกรรมแบบกรอกข้อมูลไม่สมบูรณ์    #ไม่กรอกข้อมูลผู้ขอรับสิทธิไม่ครบ
    Input Condition for Search  3210200341394  24/07/2562  1  T  11
    Input Address of Benefit Owner for submit  23/5  วิลล่าพาร์ค  4  3  รัตนาธิเบศร์  10  1030  103005  ${empty}  029686038  ${empty}  0944844151
    #รหัสไปรษณีย์ใส่ค่าอัตโนมัติหลังจากเลือก จังหวด,อำเถอ,ตำบลแล้ว กรณีที่ต้องการใส่ค่าเอง ไปยกเลิกคอมเมนที่ Keywords
    #ข้อมูลผู้ขอรับสิทธิ
    Click Element    //label[@for="requesterTypeH"]
    #Click Element    xpath=(//span[@class="filter-option pull-left"])[6]
    Select From List By Value    //select[@id="requester-type-H-entitled-new-citizenId"]    ${empty}  #ไม่เลือก ร.พ.
    #รายการรักษาที่เบิก
    Input Benefit Payment Descriptions  112000000  1  900
    
    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[5]/div/button
    Element Text Should Be  //*[@id="requester-type-H"]/div/div[2]/div[2]  โปรดระบุ


*** Keywords ***
Input Condition for Search
    [Arguments]  ${citizenId}  ${treatmentDate}  ${benefitCode}  ${claimCaseCode}  ${claimBenefitCode}
    
    Wait Until Page Contains Element  id:citizenId
    Input Text  id:citizenId  ${citizenId}

    Wait Until Page Contains Element  id:treatmentDate
    Input Text  id:treatmentDate  ${treatmentDate}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:benefitCode 
    Select From List By Value    //select[@name="petition.benefitCode"]  ${benefitCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimCaseCode
    Select From List By Value    //select[@name="petitionDetail.claimCaseCode"]  ${claimCaseCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimBenefitCode
    Select From List By Value    //select[@name="petitionDetail.claimBenefitCode"]  ${claimBenefitCode}
    Press Keys    None    ESC

    Click Element  id:search

Input Address of Benefit Owner for submit
    [Arguments]  ${homeId}  ${village}  ${moo}  ${soi}  ${road}  ${provinceCode}  ${amphurCode}  ${tambolCode}  ${postCode}  ${homePhone}  ${email}  ${mobilePhone}
    Wait Until Page Contains Element  id:homeId
    Input Text  id:homeId  ${homeId}

    Wait Until Page Contains Element  id:village
    Input Text  id:village  ${village}

    Wait Until Page Contains Element  id:moo
    Input Text  id:moo  ${moo}

    Wait Until Page Contains Element  id:soi
    Input Text  id:soi  ${soi}

    Wait Until Page Contains Element  id:road
    Input Text  id:road  ${road}

    Wait Until Page Contains Element  id:provinceCode
    Select From List By Value     //select[@name="employee.provinceCode"]  ${provinceCode}
    #รหัสจังหวัด (กทม = 10)
    #Press Keys    None    ESC

    Wait Until Page Contains Element  id:amphurCode
    Select From List By Value     //select[@name="employee.amphurCode"]  ${amphurCode}
    #รหัสอำเภอ/เขต (จตุจักร = 1030)
    #Press Keys    None    ESC

    Wait Until Page Contains Element  id:tambolCode
    Select From List By Value     //select[@name="employee.tambolCode"]  ${tambolCode}
    #รหัสตำบล/แขวง (จตุจักร = 103005)
    #Press Keys    None    ESC

    #Wait Until Page Contains Element  id:postCode
    #Input Text  id:postCode  ${postCode}

    Wait Until Page Contains Element  id:road
    Input Text  id:road  ${homePhone}

    Wait Until Page Contains Element  id:email
    Input Text  id:email  ${email}

    Wait Until Page Contains Element  id:mobilePhone
    Input Text  id:mobilePhone  ${mobilePhone}

Input Benefit Owner for Payment case:I
    [Arguments]  ${requester-type-I-entitled-new-entitledMethod}  ${requester-type-I-entitled-new-bankCode}  ${requester-type-I-entitled-new-bankAccNo}

    Select From List By Value    //select[@id="requester-type-I-entitled-new-entitledMethod"]  ${requester-type-I-entitled-new-entitledMethod}
    Press Keys    None    TAB
    #Option Value /"O" รับ ณ สำนักงาน/"P" ไปรษณีย์/"D" โอนโดยจังหวัด/"B" ธนาคาร/

    Select From List By Value    //select[@id="requester-type-I-entitled-new-bankCode"]  ${requester-type-I-entitled-new-bankCode}
    #Option Value
    #002 ธนาราคากรุงเทพ / 004 ธนาคารกสิกรไทย / 006 ธนาคารกรุงไทย / 011 ธนาคารทหารไทย / 014 ธนาคารไทยพาณิชย์ /
    #022 ธนาคารซีไอเอ็มบี / 025 ธนาคารกรุงศรีอยุธยา / 030 ธนาคารออมสิน / 034 ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร /
    #065 ธนาคารธนชาต / 066 ธนาคารอิสลามแห่งประเทศไทย
    Input Text    id:requester-type-I-entitled-new-bankAccNo  ${requester-type-I-entitled-new-bankAccNo}


Input Benefit Owner for Payment case:H
    [Arguments]  ${treatmentCode-1}

Input Benefit Owner for Payment case:B
    [Arguments]  ${homeId}

Input Benefit Payment Descriptions
    [Arguments]  ${treatmentCode-1}  ${noOfUnit-1}  ${claimAmount-1}

    Select From List By Value    //select[@id="treatmentCode-1"]  ${treatmentCode-1}
    #Option Value
    #111000000 : อุดฟัน / 111000001 : อุดฟัน AMALGAM 1 ด้าน / 111000002 : อุดฟัน AMALGAM 2 ด้าน /
    #111000003 : อุดฟันสีเหมือนฟัน 1 ด้านหน้า / 111000004 : อุดฟันสีเหมือนฟัน 1 ด้านหลัง /
    #111000005 : อุดฟันสีเหมือนฟัน 2 ด้านหน้า / 111000006 : อุดฟันสีเหมือนฟัน 2 ด้านหลัง /
    #112000000 : ขูดหินปูน / 113000000 : ถอนฟัน / 113000001 : ถอนฟันแท้ / 113000002 : ถอนฟันที่ยาก
    #114000000 : ผ่าฟันคุด
    Input Text    id:noOfUnit-1  ${noOfUnit-1}
    Input Text    id:claimAmount-1  ${claimAmount-1}
    
    Press Keys    None    TAB