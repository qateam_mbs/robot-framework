*** Setting ***
Library  SeleniumLibrary
Library  ExtendedSeleniumLib
Resource  ../resources/edentConfiguration.robot
Suite Setup     gotoProject  admin  password
#Suite Teardown  Close Browser
Test Setup  Go To  ${EDENT03001}

*** Variables ***


*** Test Cases ***
EDENT03001 Normal Search Input Data  
    [Tags]  Value1
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ
    Input Condition for Search  100062000011614  3200601163993  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3200601163993
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be Equal  '${count}'  '1'   

EDENT03001 Normal Search Input Data without petitionNo   
    [Tags]  Value1.1
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล Petition_No
    Input Condition for Search  ${Empty}  3200601163993  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3102002702172
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} > 1

EDENT03001 Normal Search Input Data without citizenId 
    [Tags]  Value1.2
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล CitizenId
    Input Condition for Search  100062000011614  ${Empty}  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr/td[1]  100062000011614
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be Equal  '${count}'  '1'

EDENT03001 Normal Search Input Data without citizenId and PetitionNo
    [Tags]  Value1.3
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล CitizenId และ เลขที่รับแจ้ง
    Input Condition for Search  ${Empty}  ${Empty}  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/thead/tr/th[1]  เลขรับแจ้ง
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} > 5

EDENT03001 Normal Search Input Data without citizenId and PetitionNo (but specify Date)
    [Tags]  Value1.4
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล CitizenId และ เลขที่รับแจ้ง
    Input Condition for Search  ${Empty}  ${Empty}  01/07/2562  02/07/2562  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/thead/tr/th[1]  เลขรับแจ้ง
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[4]  01/07/2562
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[5]  02/07/2562
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} > 5

EDENT03001 Normal Search Input Condition data found 
    [Tags]  Value2
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ กรณีไม่พบข้อมูล
    Input Condition for Search  123165485118416  3212145481231  01/07/2562  01/07/2562  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT03001 Validate Search No Data Input Found 
    [Tags]  Value3
    Comment  ทดสอบการค้นหาข้อมูล กรณีไม่กรอกข้อมูลรหัสสปส.
    Input Condition for Search  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#ssoBranchCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#benefitCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT03001 Validate Search No Data Input Found (Without ssoBranchCode)
    [Tags]  Value3.1
    Comment  ทดสอบการค้นหาข้อมูล กรณีไม่กรอกข้อมูล รหัส สปส.
    Input Condition for Search  ${Empty}  3200601163993  ${Empty}  ${Empty}  ${Empty}  1  T  11
    Element Text Should Be  //*[@id="#ssoBranchCode"]  โปรดระบุ

EDENT03001 Validate Search No Some Data Input Found (Without benefitCode,claimBenefitCode,claimCaseCode)
    [Tags]  Value3.2
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล ประเภทสิทธิฯ, กรณีรักษา, สิทธิประโยชน์ 
    Input Condition for Search  ${Empty}  3212145481231  22/06/2562  ${Empty}  1000  ${Empty}  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#benefitCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT03001 Validate Search No Some Data Input Found (Without claimBenefitCode,claimCaseCode)
    [Tags]  Value3.3
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล กรณีรักษา, สิทธิประโยชน์ 
    Input Condition for Search  ${Empty}  3212145481231  22/06/2562  ${Empty}  1000  1  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT03001 Validate Search No Some Data Input Found (Without claimBenefitCode)
    [Tags]  Value3.4
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล สิทธิประโยชน์ 
    Input Condition for Search  ${Empty}  3212145481231  22/06/2562  ${Empty}  1000  1  T  ${Empty}
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT03001 Negative Search Without Complete Data PetitionNo (Check Digi)
    [Tags]  Value4
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกข้อมูล PetitionNo ไม่ครบจำนวนที่กำหนด
    Input Condition for Search  1000620000186  3471200430639  ${Empty}  ${Empty}  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT03001 Negative Search Without Complete Data CitizenId (Check Digi)
    [Tags]  Value4.1
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล CitizenId ไม่ครบจำนวนที่กำหนด
    Input Condition for Search  100062000018676  347120043  ${Empty}  ${Empty}  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT03001 Normal Create Petition Cash Completely
    [Tags]  Value5
    Comment  ทดสอบการสร้างคำสั่งจ่ายแบบปกติ โดยไม่เลือกเลขที่คำสั่งจ่ายและเลขที่บัตรประชาชน
    Input Condition for Search  ${Empty}  ${Empty}  ${Empty}  ${Empty}  1000  1  T  11
    #Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3200601163993
    #Event นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} >= 1
    #Event คลิกสร้างคำสั่งจ่าย
    Click Element    //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[8]/a/span
    #Event ตรวจสอบคำสั่งจ่ายและยืนยัน
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div/div/div[1]/div[2]/div[1]/div[4]  A : อนุมัติแล้ว
    Input Password  //*[@id="password"]  password
    Click Element   //*[@id="page-content-wrapper"]/form/div/div/div[3]/div[2]/div[8]/div/button

EDENT03001 Normal Create Petition Cash Without Data CitizenId       #รอแก้ไขโปรแกรม
    [Tags]  Value5.1
    Comment  ทดสอบการสร้างคำสั่งจ่ายแบบปกติ แบบไม่กรอกข้อมูล เลขที่สั่งจ่ายและเลขที่บัตรประชาชน
    Input Condition for Search  100062000000036  ${Empty}  ${Empty}  ${Empty}  1000  1  T  11
    #Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/thead/tr/th[1]  เลขรับแจ้ง
    #Event นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} >= 1
    #Event คลิกสร้างคำสั่งจ่าย
    #Click Element    //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[8]/a/span

EDENT03001 Normal Create Petition Cash Without Data PetitionNo      #รอแก้ไขโปรแกรม
    [Tags]  Value5.2
    Comment  ทดสอบการสร้างคำสั่งจ่ายแบบปกติ แบบไม่กรอกข้อมูล เลขที่สั่งจ่ายและเลขที่บัตรประชาชน
    Input Condition for Search  ${Empty}  1234567890121  ${Empty}  ${Empty}  1000  1  T  11
    #Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/thead/tr/th[1]  เลขรับแจ้ง
    #Event นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} >= 1
    #Event คลิกสร้างคำสั่งจ่าย
    #Click Element    //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[8]/a/span

EDENT03001 Normal Create Petition Cash With Date    #รอแก้ไขโปรแกรม
    [Tags]  Value5.3
    Comment  ทดสอบการสร้างคำสั่งจ่ายแบบปกติ เลือกวันที่ในการค้นหา
    Input Condition for Search  ${Empty}  ${Empty}  01/07/2562  02/07/2562  1000  1  T  11
    #Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/thead/tr/th[1]  เลขรับแจ้ง
    #Event นับจำนวน Table Row ที่ระบบแสดงผล
    #${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    #Should Be True  ${count} >= 1
    #Event คลิกสร้างคำสั่งจ่าย
    #Click Element    //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[8]/a/span

DENT03001 Negative No Create Petition Cash because unknow Data
    [tags]  Value6
    Comment  ทดสอบการสร้างคำสั่งจ่ายแบบปกติ กรณีมีข้อมูลสมบูรณ์
    Input Condition for Search  ${Empty}  3260400142930  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3260400142930
    #Event นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} > 1
    #Event คลิกสร้างคำสั่งจ่าย
    Click Element  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[8]/a/span
    Element Text Should Not Be  //*[@id="system-alert-message"]/div  BEN03001_E003 : ไม่สามารถสร้างคำสั่งจ่ายได้ เนื่องจากไม่พบข้อมูลผู้รับสิทธิ

*** Keywords ***
Input Condition for Search
    [Arguments]  ${petitionNo}  ${citizenId}  ${petitionDate}  ${diagnoseDate}  ${ssoBranchCode}  ${benefitCode}  ${claimCaseCode}  ${claimBenefitCode}
    Wait Until Page Contains Element  id:petitionNo
    Input Text  id:petitionNo  ${petitionNo}

    Wait Until Page Contains Element  id:citizenId
    Input Text  id:citizenId  ${citizenId}

    Wait Until Page Contains Element  id:petitionDate
    Input Text  id:petitionDate  ${petitionDate}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:diagnoseDate
    Input Text  id:diagnoseDate  ${diagnoseDate}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:ssoBranchCode 
    Click Element    //span[@class="filter-option pull-left"]
    Select From List By Value    //select[@name="petition.ssoBranchCode"]  ${ssoBranchCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:benefitCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[2]
    Select From List By Value    //select[@name="petition.benefitCode"]  ${benefitCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimCaseCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[3]
    Select From List By Value    //select[@name="petitionDetail.claimCaseCode"]  ${claimCaseCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimBenefitCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[4]
    Select From List By Value    //select[@name="petitionDetail.claimBenefitCode"]  ${claimBenefitCode}
    Press Keys    None    ESC

    Click Element  id:search