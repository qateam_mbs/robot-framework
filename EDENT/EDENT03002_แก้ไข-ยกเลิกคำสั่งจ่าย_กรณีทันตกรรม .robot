*** Setting ***
Library  SeleniumLibrary
Library  ExtendedSeleniumLib
Resource  ../resources/edentConfiguration.robot
Suite Setup     gotoProject  admin  password
#Suite Teardown  Close Browser
Test Setup  Go To  ${EDENT03001}

*** Variables ***


*** Test Cases ***
EDENT03001 Normal Search Input Data  
    [Tags]  Value1
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ
    Input Condition for Search  100062000013768  3102002702172  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3102002702172
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be Equal  '${count}'  '1'    

EDENT03001 Normal Search Input Data but none petitionNo   
    [Tags]  Value1.1
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล Petition_No
    Input Condition for Search  ${Empty}  3102002702172  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3102002702172
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} > 1

EDENT03001 Normal Search Input Data but none citizenId 
    [Tags]  Value1.2
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ แต่ไม่ใส่ข้อมูล CitizenId
    Input Condition for Search  100062000013768  ${Empty}  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3102002702172
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be Equal  '${count}'  '1'

EDENT03001 Normal Search Input Condition with no data found  
    [Tags]  Value2
    Comment  ทดสอบการค้นหาข้อมูลแบบปกติ กรณีไม่พบข้อมูล
    Input Condition for Search  123165485118416  3212145481231  01/07/2562  01/07/2562  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT03001 Negative Search No Data Input Found 
    [Tags]  Value3
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกข้อมูล
    Input Condition for Search  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#ssoBranchCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#benefitCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT03001 Negative Search No Some Data Input Found 
    [Tags]  Value3.1
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล ประเภทสิทธิฯ, กรณีรักษา, สิทะิประโยชน์ 
    Input Condition for Search  ${Empty}  3212145481231  22/06/2562  ${Empty}  1000  ${Empty}  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#benefitCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT03001 Negative Search No Some Data Input Found 
    [Tags]  Value3.2
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล กรณีรักษา, สิทะิประโยชน์ 
    Input Condition for Search  ${Empty}  3212145481231  22/06/2562  ${Empty}  1000  1  ${Empty}  ${Empty}
    Element Text Should Be  //*[@id="#claimCaseCode"]  โปรดระบุ
    Element Text Should Be  //*[@id="#claimBenefitCode"]  โปรดระบุ

EDENT03001 Negative Search Without Complete Data PetitionNo
    [Tags]  Value4
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกข้อมูล PetitionNo ไม่ครบ
    Input Condition for Search  1000620000186  3471200430639  ${Empty}  ${Empty}  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT03001 Negative Search Without Complete Data CitizenId
    [Tags]  Value4.1
    Comment  ทดสอบการค้นหาข้อมูลแบบไม่ปกติ กรณีไม่กรอกบ้อมูล CitizenId ไม่ครบ
    Input Condition for Search  100062000018676  347120043  ${Empty}  ${Empty}  1000  1  T  12
    Element Text Should Not Be  //*[@id="system-alert-message"]  I001 : ไม่พบข้อมูล

EDENT03001 Normal Create Petition Cash Completely
    [tags]  Value5
    Comment  ทดสอบการสร้างคำสั่งจ่ายแบบปกติ กรณีมีข้อมูลสมบูรณ์
    Input Condition for Search  ${Empty}  3260400142930  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3102002702172
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} > 1
    Click Element    //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[8]/a/span

DENT03001 Normal No Create Petition Cash because unknow Data
    [tags]  Value6
    Comment  ทดสอบการสร้างคำสั่งจ่ายแบบปกติ กรณีมีข้อมูลสมบูรณ์
    Input Condition for Search  ${Empty}  3260400142930  ${Empty}  ${Empty}  1000  1  T  11
    Element Text Should Be  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[2]  3260400142930
    #นับจำนวน Table Row ที่ระบบแสดงผล
    ${count}=  Get Element Count  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr
    Should Be True  ${count} > 1
    Click Element  //*[@id="page-content-wrapper"]/form/div[2]/div/div[1]/table/tbody/tr[1]/td[8]/a/span
    Element Text Should Not Be  //*[@id="system-alert-message"]/div  BEN03001_E003 : ไม่สามารถสร้างคำสั่งจ่ายได้ เนื่องจากไม่พบข้อมูลผู้รับสิทธิ

*** Keywords ***
Input Condition for Search
    [Arguments]  ${petitionNo}  ${citizenId}  ${petitionDate}  ${diagnoseDate}  ${ssoBranchCode}  ${benefitCode}  ${claimCaseCode}  ${claimBenefitCode}
    Wait Until Page Contains Element  id:petitionNo
    Input Text  id:petitionNo  ${petitionNo}

    Wait Until Page Contains Element  id:citizenId
    Input Text  id:citizenId  ${citizenId}

    Wait Until Page Contains Element  id:petitionDate
    Input Text  id:petitionDate  ${petitionDate}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:diagnoseDate
    Input Text  id:diagnoseDate  ${diagnoseDate}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:ssoBranchCode 
    Click Element    //span[@class="filter-option pull-left"]
    Select From List By Value    //select[@name="petition.ssoBranchCode"]  ${ssoBranchCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:benefitCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[2]
    Select From List By Value    //select[@name="petition.benefitCode"]  ${benefitCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimCaseCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[3]
    Select From List By Value    //select[@name="petitionDetail.claimCaseCode"]  ${claimCaseCode}
    Press Keys    None    ESC

    Wait Until Page Contains Element  id:claimBenefitCode
    Click Element    xpath=(//span[@class="filter-option pull-left"])[4]
    Select From List By Value    //select[@name="petitionDetail.claimBenefitCode"]  ${claimBenefitCode}
    Press Keys    None    ESC

    Click Element  id:search