*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/ssoConfiguration.robot
Suite Setup     gotoProject&SelectModule  admin  password  ประโยชน์
Suite Teardown  Close Browser
Test Setup  Go To  ${ben02001}

*** Variables ***


*** Test Cases ***
BEN02001A_Nor_Manager_Search Data on Condition_Found
	Input Condition for Search  1234567890121  ${EMPTY}   ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
	rowsOfSearchResult  1

BEN02001A_Neg_Manager_Search Data on Condition_Not Found
	Input Condition for Search  9999999999999  ${EMPTY}   ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${citizenId}  ${petitionDate}  ${petitionNo}  ${benefitCode}  ${claimCaseCode}  ${claimBenefitCode}
    Input Text    id:citizenId    ${citizenId}
	Input Text    id:petitionDate    ${petitionDate}
	Input Text    id:petitionNo    ${petitionNo}
    Select From List By Value  name:petition.benefitCode  ${benefitCode}
	Select From List By Value  name:claimBenefit.claimCaseCode  ${claimCaseCode}
	Select From List By Value  name:claimBenefit.claimBenefitCode  ${claimBenefitCode}
    Click Element  id:search
	Loading Screen

	#ประเภทสิทธิประโยชน์ petition.benefitCode   1
	#	1 เจ็บป่วย
	#	2 คลอดบุตร
	#	3 ทุพพลภาพ
	#	4 ตาย
	#	5 สงเคราะห์บุตร
	#	6 ชราภาพ
	#กรณีรักษา claimBenefit.claimCaseCode
	# 1 เจ็บป่วย
	# -	E	ฉุกเฉิน
	# -	A	อุบัติเหตุ
	# -	T	ทันตกรรม
	# -	O	เจ็บป่วยเรื้อรัง
	# -	N	เจ็บป่วยธรรมดา
	# -	K	ฟอกเลือดตามประกาศ
	# -	R	ศูนย์ฟื้นฟู
	# -	P	ล้างช่องท้อง
	# -	J	ยาฉีด Erythropoietin
	# -	V	เส้นเลือดสำหรับฟอก
	# -	L	ท่อรับส่งน้ำยาล้าง
	# -	X	เรื้อรังเกิน 180 วัน
	# -	S	สิทธิพิเศษ
	# -	W	ครรภ์
	# 2 คลอดบุตร
	# -	M	คลอดบุตร
	# 3 ทุพพลภาพ
	# -	I	ทุพพลภาพ
	# 4 ตาย
	# -	D	ตาย
	#สิทธิประโยชน์ claimBenefit.claimBenefitCode
	# CLAIM_BENEFIT_CODE	| BENEFIT_CODE	| CLAIM_CASE_CODE 	| CLAIM_BENEFIT_NAME
	# 11					|1				|T					|ค่าอุดฟัน ถอนฟัน ขูดหินปูน
	# 12					|1				|T					|ฟันเทียมฐานอคริลิก
	# 13					|1				|T					|ฟันเทียมชนิดถอดได้บางส่วน
	# 14					|1				|T					|ฟันเทียมชนิดถอดได้ทั้งปาก
	# 72					|1				|T					|ค่าอุปกรณ์ เครื่องมือ ดัดฟัน
	# 01					|1				|					|ค่าบริการทางการแพทย์
	# 02					|1				|					|ค่าอวัยวะเทียม/อุปกรณ์บำบัดโรค
	# 03					|1				|					|เงินทดแทนการขาดรายได้
	# 15					|1				|					|ฟอกเลือดด้วยเครื่องไตเทียม
	# 16					|1				|					|ล้างช่องท้องด้วยน้ำยาแบบถาวร
	# 17					|1				|					|ค่ายาฉีด ERYTHROPOIETIN
	# 18					|1				|					|ค่าเตรียมเส้นเลือดสำหรับฟอกเลือด
	# 19					|1				|					|ค่าวางท่อรับส่งน้ำยาล้างช่องท้อง
	# 20					|1				|					|ฟอกเลือดในภาวะอุทกภัย
	# 24					|1				|					|เงินช่วยเหลือเบื้องต้น
	# 42					|1				|					|ค่าส่งเสริมสุขภาพหญิงตั้งครรภ์
	# 04					|2				|					|ค่าคลอดบุตร
	# 05					|2				|					|เงินสงเคราะห์การหยุดงานเพื่อการคลอดบุตร
	# 41					|2				|					|ค่าตรวจและรับฝากครรภ์
	# 06					|3				|					|เงินทดแทนการขาดรายได้กรณีทุพพลภาพ
	# 31					|3				|					|ค่าบริการทางการแพทย์กรณีทุพพลภาพ
	# 32					|3				|					|ค่าอวัยวะเทียม/อุปกรณ์บำบัดโรค
	# 33					|3				|					|เงินทดแทนการขาดรายได้กรณีทุพพลภาพ
	# 34					|3				|					|ค่าทำศพกรณีทุพพลภาพ
	# 35					|3				|					|เงินสงเคราะห์กรณีทุพพลภาพ
	# 36					|3				|					|ค่าฟื้นฟูสมรรถภาพ
	# 37					|3				|					|ค่าบริการทางการแพทย์กรณีทุพพลภาพใหม่
	# 38					|3				|					|ค่าบริการทางการแพทย์กรณีทุพพลภาพDRG
	# 39					|3				|					|ค่ารถพยาบาลหรือค่าพาหนะรับส่งผู้ทุพพภาพ
	# 46					|3				|					|ค่าบำบัดรักษาและการผ่าตัดฟื้นฟูสมรรถภาพ
	# 47					|3				|					|ค่าเวชศาสตร์ฟื้นฟูสมรรถภาพ ้นฟูสมรรถภาพ
	# 48					|3				|					|ค่าฟื้นฟูสมรรถภาพทางอาชีพ
	# 49					|3				|					|ค่าวัสดุอุปกรณ์ด้านเวชศาสตร์ฟื้นฟู
	# 07					|4				|					|ค่าทำศพ
	# 08					|4				|					|เงินสงเคราะห์กรณีตาย
	# 51					|5				|					|เงินสงเคราะห์บุตร
	# 61					|6				|					|บำนาญชราภาพ
	# 62					|6				|					|บำเหน็จชราภาพกรณีตาย
	# 63					|6				|					|บำเหน็จชราภาพกรณีทุพพลภาพ
	# 64					|6				|					|เงินบำเหน็จชราภาพกรณีความเป็นผปต.สิ้นสุด
	# 71					|7				|					|เงินประโยชน์ทดแทนกรณีว่างงาน
