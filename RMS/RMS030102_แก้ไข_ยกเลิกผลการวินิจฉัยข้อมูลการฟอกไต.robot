*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/rmsConfiguration.robot
Suite Setup     gotoProject&SelectModule  TEST01  TEST01  ระบบไตเทียม
Suite Teardown  Close Browser
Test Setup  Go To  ${RMS030102}

*** Variables ***


*** Test Cases ***
RMS030102A_Nor_Manager_Search Data on Condition_Found
	Input Condition for Search  ${EMPTY}  ${EMPTY}  A
	rowsOfSearchResult  33

RMS030102A_Neg_Manager_Search Data on Condition_Not Found
	Input Condition for Search  1234  ${EMPTY}  A
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${periodNumber}  ${hcode}  ${diagnoseStatus}
	Wait Until Page Contains Element    id:periodNumber
    Input Text    id:periodNumber    ${periodNumber}
	Wait Until Page Contains Element    id:hcode
	Input Text    id:hcode    ${hcode}
	Wait Until Page Contains Element    id:diagnoseStatus
    Select From List By Value  id:diagnoseStatus  ${diagnoseStatus}
    Click Element  id:search
