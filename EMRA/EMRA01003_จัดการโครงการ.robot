*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/emraConfiguration.robot
Suite Setup     gotoProject  kvarayoot  password
Suite Teardown  Close Browser
Test Setup  Go To  ${EMRA01003}

*** Variables ***


*** Test Cases ***
EMRA01003A_Nor_Manager_Search Data on Condition_Found
	Input Condition for Search  2560  ${EMPTY}  DRG
	rowsOfSearchResult  3

EMRA01003A_Neg_Manager_Search Data on Condition_Not Found
	Input Condition for Search  2562  ${EMPTY}  DRG
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${auditYear}  ${projectName}  ${projectType}
    Input Text    id:auditYear    ${auditYear}
	Input Text    id:projectName    ${projectName}
    Select From List By Value  id:projectType  ${projectType}
    Click Element  id:search
