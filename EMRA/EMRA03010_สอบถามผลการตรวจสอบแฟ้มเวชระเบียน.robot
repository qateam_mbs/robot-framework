*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/emraConfiguration.robot
Suite Setup     gotoProject  kvarayoot  password
#Suite Teardown  Close Browser
Test Setup  Go To  ${EMRA03010}

*** Variables ***


*** Test Cases ***
EMRA03010A_Nor_Anyone_Search_on Condition_Found
	[Tags]  Unit  UAT  Flow
	Input Condition for Search  NCD61010  10666  ${EMPTY}
	rowsOfSearchResult  2

EMRA03010A_Neg_Anyone_Search_on Condition_Not Found
	Input Condition for Search  NCD61010  10666  5
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${projectCode}  ${hcode5}  ${propertyCode}
	Wait Until Page Contains Element    id:projectCode
    Select From List By Value  id:projectCode  ${projectCode}
	Wait Until Page Contains Element    //*[@id="hcode5"]/option[@value="${hcode5}"]
    Select From List By Value  id:hcode5  ${hcode5}
	Wait Until Page Contains Element    id:propertyCode
    Select From List By Value  id:propertyCode  ${propertyCode}
    Click Element  id:search
