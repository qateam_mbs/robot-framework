*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/emraConfiguration.robot
Suite Setup     gotoProject  osomchai  password
Suite Teardown  Close Browser
Test Setup  Go To  ${EMRA03003}

*** Variables ***


*** Test Cases ***
EMRA03003A_Nor_Auditor DRG_Search Data on Condition_Found
	[Tags]  Unit  UAT  Flow
    Input Condition for Search  DRG60001  ${EMPTY}  ${EMPTY}
	rowsOfSearchResult  14

EMRA03003A_Neg_Auditor DRG_Search Data on Condition_Not Found
	Input Condition for Search  DRG60001  ${EMPTY}  HR1
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${projectCode}  ${hcode5}  ${status}
    Select From List By Value  id:projectCode  ${projectCode}
	Wait Until Page Contains Element    //*[@id="hcode5"]/option[@value="${hcode5}"]
    Select From List By Value  id:hcode5  ${hcode5}
    Select From List By Value  id:status  ${status}
    Click Element  id:search
