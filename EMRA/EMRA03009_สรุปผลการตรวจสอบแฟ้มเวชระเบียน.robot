*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/emraConfiguration.robot
Suite Setup     gotoProject  kvarayoot  password
Suite Teardown  Close Browser
Test Setup  Go To  ${EMRA03009}

*** Variables ***


*** Test Cases ***
EMRA03009A_Nor_Manager_Search Data on Condition_Found
	[Tags]  Unit  UAT  Flow
	Input Condition for Search  DRG61008  10823
	rowsOfSearchResult  1

EMRA03009A_Neg_Manager_Search Data on Condition_Not Found
	Input Condition for Search  DRG61008  10749
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${projectCode}  ${hcode5}
	Wait Until Page Contains Element    id:projectCode
    Select From List By Value  id:projectCode  ${projectCode}
	Wait Until Page Contains Element    //*[@id="hcode5"]/option[@value="${hcode5}"]
    Select From List By Value  id:hcode5  ${hcode5}
    Click Element  id:search
