*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spd
Suite Teardown  Close Browser
Test Setup  Go To  ${SPD0301}

*** Test Cases ***
SPD03001A_Validate_Digit citizenId_Invalid
    Comment    ใส่เลขบัตรประชาชนไม่ครบ 13 หลัก
    [Tags]  Unit
    Input Condition for Search  1234  ${EMPTY}  ${EMPTY}  1000
    Element Text Should Be  id:spanCitizenId  เลขประจำตัวประชาชนไม่ถูกต้อง

SPD03001A_Validate_Digit pettitionNo_Invalid
    Comment    ใส่เลขรับแจ้งไม่ครบ 15 หลัก
    [Tags]  Unit
    Input Condition for Search  ${EMPTY}  12345  ${EMPTY}  1000
    Element Text Should Be  id:spanPettitionNo  เลขที่รับแจ้งไม่ถูกต้อง

SPD03001A_Validate_ssoBranchCode_Require Field Not Empty
    Comment    ไม่ใส่ข้อมูลที่ require field
    [Tags]  Unit
    Input Condition for Search  ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Element Text Should Be  id:$ssoBranchCode  กรุณาเลือกข้อมูล

SPD03001A_Neg_BEN_Search Data on Condition_Not Found
    Comment    ค้นหาข้อมูลไม่พบตามเงื่อนไข
    [Tags]  test
    Input Condition for Search  ${EMPTY}  ${EMPTY}  ${EMPTY}  1000
    SearchNotFound

SPD03001A_Nor_BEN_Search_Data on Condition_Found
    Comment    ค้นหาข้อมูลพบตามเงื่อนไข
    [Tags]  Unit  UAT  Flow  test
    Input Condition for Search  ${EMPTY}  ${EMPTY}  ${EMPTY}  1005
    rowsOfSearchResult  3

SPD03001B_Nor_BEN_Save_Diag to Approve_Success
    Comment    วินิจฉัยมีสิทธิ
    [Tags]  Unit  UAT  Flow
    Input Condition for Search  3100502964794  ${EMPTY}  ${EMPTY}  1002
    Input Detail & submit  2  7  A  ทดสอบบันทึกวินิจฉัย มีสิทธิ SPD บำนาญชราภาพ  password
    #result บันทึกสำเร็จ

SPD03001B_Nor_BEN_Save_Diag to Not Approve_Success
    Comment    วินิจฉัยไม่มีสิทธิ
    [Tags]  Unit  UAT  Flow
    Input Condition for Search  3100502964794  ${EMPTY}  ${EMPTY}  1002
    Input Detail & submit  2  7  N  ทดสอบบันทึกวินิจฉัย ไม่มีสิทธ SPD บำนาญชราภาพ  password
    #result บันทึกสำเร็จ

*** Keywords ***
Input Condition for Search
    [Arguments]  ${citizenId}  ${pettitionNo}  ${pettitionDate}  ${ssoBranchCode}
    Input Text    id:citizenId    ${citizenId}
    Input Text    id:pettitionNo  ${pettitionNo}
    Input Text    id:pettitionDate  ${pettitionDate}
    Select From List By Value  id:ssoBranchCode  ${ssoBranchCode}
    Click Element  //span[@class="ui-button-text"]

Input Detail & submit
    [Arguments]  ${row}  ${column}  ${diagStatus}  ${comment}  ${pass}
    Click Element    //tbody/tr[${row}]/td[${column}]
    Wait Until Page Contains Element    name:diagnoseStatus
    Select Radio Button  diagnoseStatus  ${diagStatus}
    Wait Until Page Contains Element  id:checkData
    Select Checkbox  id:checkData
    Input Text  //textarea[@name="diagComment"]   ${comment}
    Input Password    id:password    ${pass}
    #Click Button  id:save
