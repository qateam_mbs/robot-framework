*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdConfiguration.robot
#Resource  ../resources/Validate.robot
Suite Setup     gotoProject&SelectModule  user  spd
# Suite Teardown  Close Browser
Test Setup  Go To  ${SPD0102}

*** Test Cases ***
SPD01002A_Nor_BEN_Search_Data on Condition_Found
    Input Condition for Search  ${EMPTY}  ${EMPTY}  A  ${EMPTY}  1200
    test loop table
*** Keywords ***
Input Condition for Search
    [Arguments]  ${citizenId}  ${pettitionNo}  ${pettitionStatus}  ${pettitionDate}  ${ssoBranchCode}
    Input Text    id:citizenId    ${citizenId}
    Input Text    id:pettitionNo  ${pettitionNo}
    Select From List By Value  id:pettitionStatus  ${pettitionStatus}
    Input Text    id:pettitionDate  ${pettitionDate}
    Select From List By Value  id:ssoBranchCode  ${ssoBranchCode}
    Click Element  id:search

test loop table
    ${fila} =    Get Element Count   xpath=//*[@id="searchForm"]/table[2]/tbody/tr
    :FOR    ${rowindex}    IN RANGE    1    ${fila + 1}
    \    ${present} =  Run Keyword And Return Status    Element Text Should Be  xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[2]  3120101596794
    \    log to console  ${present}
    \    Run Keyword If    ${present}    Click Element    xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[6]/a
    \    Run Keyword If    ${present}    Exit For Loop
