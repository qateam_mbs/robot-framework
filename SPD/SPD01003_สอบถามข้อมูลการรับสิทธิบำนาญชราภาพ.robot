*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdConfiguration.robot
Resource  ../resources/Validate.robot
Suite Setup     gotoProject&SelectModule  user  spd
#Suite Teardown  Close Browser
Test Setup  Go To  ${SPD0103}

*** Test Cases ***
SPD01003A_Validate_CitizenId should not Empty_warning
    Comment    ไม่ระบุเลขที่บัตรประชาชน, ไม่ระบุเลขที่บัตรประชาชน กดปุ่มค้นหาต้องแสดงข้อความแจ้งเตือน
    Input Condition for Search  ${EMPTY}
    Element Text Should Be  id:#citizenId  กรุณากรอกข้อมูล

SPD01003A_Nor_BEN_Search Data on Condition_Found
    Comment    แสดงผลรายการข้อมูล ตามที่ค้นหา, นำข้อมูลจากตาราง SPD_TR_PETTITION,SPD_TR_EMPLOYEE,SPD_TR_DIAGNOSE ที่มี มาแสดงรายการ
    Input Condition for Search  2130500016122
    Element Text Should Be  xpath://table[2]/tbody/tr[2]/td[4]  2130500016122

SPD01003A_Neg_BEN_Search Data on Condition_Not Found
    Comment    กรณีค้นหาข้อมูลไม่พบ, ระบุเลขประจำตัวประชาชน หรือ เลขที่คำสั่งจ่าย และ สปส.รับผิดชอบ ไม่ตรง ระบบแจ้งเตือน
    Input Condition for Search  1409900185140
    SearchNotFound

SPD01003A_Validate_Check last digit citizenId_Invalid
    Check last digit    1234567890123    ${False}

SPD01003A_Validate_Check last digit citizenId_valid
    Check last digit    2130500016122    ${True}

SPD01003A_Validate_Check last digit citizenId_valid2
    Check last digit    3100501972602    ${True}

SPD01003A_Validate_Check digit citizenId not complete digits_Invalid
        [Tags]  Test
        Comment    เลขบัตรประชาชน ไม่ครบ 13 หลัก ต้องแสดงข้อความแจ้งเตือน
        Input Condition for Search  31005019726
        Element Text Should Be  เลขประจำตัวประชาชนไม่ถูกต้อง

SPD01003A_Validate_Charactor not match with citizenId_Invalid

        Comment    ช่องเลขประจำตัวประชาชนพิมพ์ตัวอักษรไม่ได้
        Input Condition for Search  บัตรประชาชน


*** Keywords ***
Input Condition for Search
    [Arguments]  ${citizenId}
    Wait Until Page Contains Element    id:citizenId
    Input Text    id:citizenId    ${citizenId}
    Click Element  id:search

View Detail
    [Arguments]  ${pettitionNo}  ${searchColumn}  ${clickColumn}
    ${rows} =    Get Element Count   xpath=//*[@id="searchForm"]/table[2]/tbody/tr
    :FOR    ${rowindex}    IN RANGE    1    ${rows + 1}
    \    ${present} =  Run Keyword And Return Status    Element Text Should Be  xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[${searchColumn}]  ${citizenId}
    \    Run Keyword If    ${present}    Click Element    xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[${clickColumn}]/a
    \    Run Keyword If    ${present}    Exit For Loop
    Wait Until Page Contains Element  xpath://div[contains(text(), "SPD01002B")]
