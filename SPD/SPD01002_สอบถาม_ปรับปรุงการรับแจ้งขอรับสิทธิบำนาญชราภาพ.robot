*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdConfiguration.robot
Resource  ../resources/Validate.robot
Suite Setup     gotoProject&SelectModule  user  spd
#Suite Teardown  Close Browser
Test Setup  Go To  ${SPD0102}

*** Test Cases ***
#test
#test1
SPD01002A_Nor_BEN_Search SSO & Status A on Condition_Found
    Comment    แสดงผลรายการ สถานะรับแจ้ง : A อนุมัติ และ สปส. ที่รับผิดชอบ, นำข้อมูลจากตาราง SPD_TR_PETTITION ที่มี PETTITION_STATUS = 'A' มาแสดงรายการ
    Input Condition for Search  ${EMPTY}  ${EMPTY}  A  ${EMPTY}  1200
    rowsOfSearchResult  2899

SPD01002A_Neg_BEN_Search Data on Condition_Not Found
    Comment    กรณีค้นหาข้อมูลไม่พบ, เลือกวันที่รับแจ้ง หรือ สปส.รับผิดชอบ ไม่ตรง ระบบแจ้งเตือน
    Input Condition for Search  3101201220090  ${EMPTY}  A  ${EMPTY}  1200
    SearchNotFound

SPD01002A_Nor_BEN_View Data on Search Condition_Show detail
    [Tags]  Test
    Comment  การแสดงผลข้อมูล, บันทึกข้อมูล ผู้ประกันตน วิธีการรับสิทธิ
    Input Condition for Search  ${EMPTY}  ${EMPTY}  A  ${EMPTY}  1200
    ${row}=  Find in rows    3120101596794    2
    Log To Console    \n row:${row}
    ${row}=  Find in rows    3120101596793    2
    Log To Console    \n row:${row}
    #View Detail    3120101596794    2    6
    #Page Should Contain  3120101596794

SPD01002A_Validate_Check last digit citizenId_Invalid
    Check last digit    1234567890123    ${False}

SPD01002A_Validate_Check last digit citizenId_valid
    Check last digit    1409900185140    ${True}

SPD01002A_Validate_Check last digit citizenId_valid2
    Check last digit    1100900443771    ${True}

SPD01002A_Validate_Check digit citizenId not complete digits_Invalid
    [Tags]  Valva1
    Comment  ใส่เลขบัตรประชาชนไม่ครบจำนวน 13 หลัก
    Input Condition for Search  312010159679  ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Element Text Should Be  //*[@id="spanCitizenId"]  เลขประจำตัวประชาชนไม่ถูกต้อง

SPD01002A_Validate_Check digit petitionNo not complete digits_Invalid
    [Tags]  Valva2
    Comment  ใส่เลขรับแจ้งไม่ครบจำนวน 15 หลัก
    Input Condition for Search  3120101596798  123456789101  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Element Text Should Be  //*[@id="spanPettitionNo"]  เลขที่รับแจ้งไม่ถูกต้อง

SPD01002A_Validate_Charactor not match with citizenId_Invalid
    [Tags]  Valva3
    Comment  ใส่ตัวอักษรในช่องกรอกข้อมูลบัตรประชาชน
    Input Condition for Search  ทดสอบการพิมพ์  ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Alert Should Be Present  text=กรุณาระบุข้อมูลที่ต้องการค้นหา  action=ACCEPT

SPD01002A_Validate_Charactor not match with petitionNo_Invalid
    [Tags]  Valva4
    Comment  ใส่ตัวอักษรในช่องกรอกข้อมูลเลขที่รับแจ้ง
    Input Condition for Search  ${EMPTY}  ทดสอบการพิมพ์  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Alert Should Be Present  text=กรุณาระบุข้อมูลที่ต้องการค้นหา  action=ACCEPT

SPD01002A_Neg_BEN_Search SSO not Empty & no Condition others_Warning
    [Tags]  Valva5
    Comment  ไม่ระบุ สปส. ที่รับผิดชอบในช่องกรอกข้อมูล
    Input Condition for Search  3100502626201  100248795510214  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Element Text Should Be  //*[@id="msg"]/div/p  ไม่พบข้อมูลที่ต้องการค้นหา กรุณาตรวจสอบเงื่อนไขที่ระบุ

SPD01002A_Nor_BEN_Search SSO not Empty & at least one Condition_Found
    [Tags]  Valva6
    Comment  ไม่ระบุ สปส. ที่รับผิดชอบในช่องกรอกข้อมูล
    Input Condition for Search  ${EMPTY}  100248795510214  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Element Text Should Be  //*[@id="msg"]/div/p  ไม่พบข้อมูลที่ต้องการค้นหา กรุณาตรวจสอบเงื่อนไขที่ระบุ

SPD01002A_Nor_BEN_Search SSO Empty & at least one Condition_Found
    [Tags]  Valva7
    Comment  ไม่ระบุ สปส. ที่รับผิดชอบในช่องกรอกข้อมูล
    Input Condition for Search  ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
    Alert Should Be Present  text=กรุณาระบุข้อมูลที่ต้องการค้นหา  action=ACCEPT

SPD01002A_Nor_BEN_Search SSO True input
    [Tags]  Valva8
    Comment  ระบุ สปส. ที่รับผิดชอบในช่องกรอกข้อมูล
    Input Condition for Search  3100502626201  100248795510214  ${EMPTY}  ${EMPTY}  1002
    #Alert Should Be Present  text=กรุณาระบุข้อมูลที่ต้องการค้นหา  action=ACCEPT

*** Keywords ***
Input Condition for Search
    [Arguments]  ${citizenId}  ${pettitionNo}  ${pettitionStatus}  ${pettitionDate}  ${ssoBranchCode}
    Wait Until Page Contains Element    id:citizenId
    Input Text    id:citizenId    ${citizenId}
    Wait Until Page Contains Element    id:pettitionNo
    Input Text    id:pettitionNo  ${pettitionNo}
    Wait Until Page Contains Element    id:pettitionStatus
    Select From List By Value  id:pettitionStatus  ${pettitionStatus}
    Wait Until Page Contains Element    id:pettitionDate
    Input Text    id:pettitionDate  ${pettitionDate}
    Wait Until Page Contains Element    id:ssoBranchCode
    Select From List By Value  id:ssoBranchCode  ${ssoBranchCode}
    Click Element  id:search

View Detail
    [Arguments]  ${citizenId}  ${searchColumn}  ${clickColumn}
    ${rows} =    Get Element Count   xpath=//*[@id="searchForm"]/table[2]/tbody/tr
    :FOR    ${rowindex}    IN RANGE    1    ${rows + 1}
    \    ${present} =  Run Keyword And Return Status    Element Text Should Be  xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[${searchColumn}]  ${citizenId}
    \    Run Keyword If    ${present}    Click Element    xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[${clickColumn}]/a
    \    Run Keyword If    ${present}    Exit For Loop
    Wait Until Page Contains Element  xpath://div[contains(text(), "SPD01002B")]

Find in rows
    [Arguments]  ${value}  ${searchColumn}
    ${foundRow}=  0
    ${rows} =    Get Element Count   xpath=//*[@id="searchForm"]/table[2]/tbody/tr
    :FOR    ${rowindex}    IN RANGE    1    ${rows + 1}
    \    ${result} =  Run Keyword And Return Status    Element Text Should Be  xpath=//*[@id="searchForm"]/table[2]/tbody/tr[${rowindex}]/td[${searchColumn}]  ${value}
    \    Run Keyword If    ${result}    ${foundRow}=    ${rowindex}
    \    Run Keyword If    ${result}    Exit For Loop
    Return From Keyword    ${foundRow}

#แสดงข้อมูล
#แสดงปุ่ม พิมพ์แบบรับเรื่อง / ยืนยันส่ง / แก้ไข / ยกเลิกรับแจ้ง / ย้อนกลับ
Button Print Report is Visible
    Element Should Be Visible    xpath://span[contains(text(), "พิมพ์แบบรับเรื่อง")]

Button Print Report is Not Visible
    Element Should Not Be Visible   xpath://span[contains(text(), "พิมพ์แบบรับเรื่อง")]

Button Confirm is Visible
    Element Should Be Visible    xpath://span[contains(text(), "ยืนยันส่ง")]

Button Confirm is Not Visible
    Element Should Not Be Visible    xpath://span[contains(text(), "ยืนยันส่ง")]

Button Edit is Visible
    Element Should Be Visible    xpath://span[contains(text(), "แก้ไข")]

Button Edit is Not Visible
    Element Should Not Be Visible    xpath://span[contains(text(), "แก้ไข")]

Button Cancel is Visible
    Element Should Be Visible    xpath://span[contains(text(), "ยกเลิกรับแจ้ง")]

Button Cancel is Not Visible
    Element Should Not Be Visible    xpath://span[contains(text(), "ยกเลิกรับแจ้ง")]

Button Back is Visible
    Element Should Be Visible    xpath://span[contains(text(), "ย้อนกลับ")]

Button Back is Not Visible
    Element Should Not Be Visible    xpath://span[contains(text(), "ย้อนกลับ")]
