*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/spdConfiguration.robot
Suite Setup     gotoProject&SelectModule  user  spd
Suite Teardown  Close Browser
Test Setup  Go To  ${ADM0302}

*** Test Cases ***
ADM0302A_Nor_BEN_Search_Data on Condition_Found
    [Tags]  test
    Input Condition for Search  ${EMPTY}  ${EMPTY}
    rowsOfSearchResult  165

ADM0302A_Neg_BEN_Search_Data on Condition_Not Found
    [Tags]  test
    Input Condition for Search  ${EMPTY}  sdfsdfsdfsdf
    SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${ssoBranchCode}  ${ssoBranchName}
    Input Text    id:ssoBranchCode    ${ssoBranchCode}
    Input Text    id:ssoBranchName  ${ssoBranchName}
    Click Button    id:search
#test
#test Ana