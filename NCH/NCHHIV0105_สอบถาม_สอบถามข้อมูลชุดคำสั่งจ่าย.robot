*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/nchConfiguration.robot
Suite Setup     gotoProject&SelectModule  admin  admin  HIV
#Suite Teardown  Close Browser
Test Setup  Go To  ${NCHHIV0105}

*** Variables ***


*** Test Cases ***
NCHHIV0105A_Nor_Anyone_Search_on Condition_Found
	[Tags]  Unit  UAT  Flow
	Choose Hospital  11472  ${EMPTY}  ${EMPTY}
	#11472,0210009,ราชวิถี
	#Input Condition for Search  620000066  ${EMPTY}  ${EMPTY}  ${EMPTY}
	#rowsOfSearchResult  1

NCHHIV0105A_Neg_Anyone_Search_on Condition_Not Found
	Input Condition for Search  6200000066  ${EMPTY}  ${EMPTY}  ${EMPTY}
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${payOrderSetNo}  ${periodNo}  ${docNo}  ${approveStatus}
	Wait Until Page Contains Element    id:payOrderSetNo
	Input Text    id:payOrderSetNo    ${payOrderSetNo}
	Wait Until Page Contains Element	id:periodNo
	Input Text    id:periodNo    ${periodNo}
	Wait Until Page Contains Element	id:docNo
	Input Text    id:periodNo    ${docNo}
	Wait Until Page Contains Element  id:approveStatus
    Select From List By Value  id:approveStatus  ${approveStatus}
    Click Element  id:search

Choose Hospital
	[Arguments]		${hcode5}	${hcode7}	${hname}
	Click Element  xpath://span[@class='ui-button-text']
	#Select Window
	Wait Until Page Contains Element    id:hcode5
	Input Text    id:hcode5    ${hcode5}
	Wait Until Page Contains Element    id:hcode7
	Input Text    id:hcode7    ${hcode7}
	Wait Until Page Contains Element    id:hname
	Input Text    id:hname    ${hname}
	Click Element  id:search
