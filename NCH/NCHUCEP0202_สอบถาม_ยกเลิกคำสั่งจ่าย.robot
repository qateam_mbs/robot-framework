*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/nchConfiguration.robot
Suite Setup     gotoProject&SelectModule  admin  admin  เจ็บป่วยฉุกเฉินวิกฤต
Suite Teardown  Close Browser
Test Setup  Go To  ${NCHUCEP0202}

*** Variables ***


*** Test Cases ***
NCHUCEP0202A_Nor_Anyone_Search on Condition_Found
	[Tags]  Unit  UAT  Flow
	Input Condition for Search   ${EMPTY}  ${EMPTY}  ${EMPTY}  2274013  ${EMPTY}  ${EMPTY}
	rowsOfSearchResult  25

NCHUCEP0202A_Neg_Anyone_Search on Condition_Not Found
	Input Condition for Search   ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}  N  ${EMPTY}
	SearchNotFound


*** Keywords ***
Input Condition for Search
    [Arguments]  ${payOrderNo}  ${periodNo}  ${hcode5}  ${hcode7}  ${approveStatus}  ${payOrderStatus}
	Wait Until Page Contains Element    id:payOrderNo
	Input Text    id:payOrderNo    ${payOrderNo}
	Wait Until Page Contains Element	id:periodNo
	Input Text    id:periodNo    ${periodNo}
	Wait Until Page Contains Element	id:hcode5
	Input Text    id:hcode5    ${hcode5}
	Wait Until Page Contains Element	id:hcode7
	Input Text    id:hcode7    ${hcode7}
	Wait Until Page Contains Element  id:approveStatus
    Select From List By Value  id:approveStatus  ${approveStatus}
	Wait Until Page Contains Element  id:payOrderStatus
    Select From List By Value  id:payOrderStatus  ${payOrderStatus}
    Click Element  id:search
