*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/nchConfiguration.robot
Suite Setup     gotoProject&SelectModule  admin  admin  Administrator/Import File Banks
Suite Teardown  Close Browser
Test Setup  Go To  ${NCHMAS0102}

*** Variables ***


*** Test Cases ***
NCHMAS0102_Nor_Anyone_Search_on Condition_Found
	[Tags]  Unit  UAT  Flow
	Input Condition for Search  11982  ${EMPTY}  ${EMPTY}
	rowsOfSearchResult  1

NCHMAS0102_Neg_Anyone_Search_on Condition_Not Found
	Input Condition for Search  99999  ${EMPTY}  ${EMPTY}
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${hcode5}  ${hcode7}  ${hname}
	Wait Until Page Contains Element    id:hcode5
	Input Text    id:hcode5    ${hcode5}
	Wait Until Page Contains Element	id:hcode7
	Input Text    id:hcode7    ${hcode7}
	Wait Until Page Contains Element	id:hname
	Input Text    id:hname    ${hname}
    Click Element  id:search
