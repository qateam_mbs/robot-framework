*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/nchConfiguration.robot
Suite Setup     gotoProject&SelectModule  admin  admin  เจ็บป่วยฉุกเฉินวิกฤต
#Suite Teardown  Close Browser
Test Setup  Go To  ${NCHHIV0202}

*** Variables ***


*** Test Cases ***
NCHHIV0202A_Nor_Anyone_Search on Condition_Found
	Choose Hospital  ${EMPTY}  ${EMPTY}  ${EMPTY}  NCHHIV0202A  NCHPOPUP03
	#11472
	#Input Condition for Search   62R000002  ${EMPTY}  ${EMPTY}
	#rowsOfSearchResult  1

#NCHUCEP0202A_Neg_Anyone_Search on Condition_Not Found
#	Input Condition for Search   ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}  N  ${EMPTY}
#	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${hivDataSet}  ${statementNo}  ${docNo}
	Wait Until Page Contains Element    id:hivDataSet
	Input Text    id:hivDataSet    ${hivDataSet}
	Wait Until Page Contains Element	id:statementNo
	Input Text    id:statementNo    ${statementNo}
	Wait Until Page Contains Element	id:docNo
	Input Text    id:docNo    ${docNo}
    Click Element  id:search

Choose Hospital
	[Arguments]  ${hcode5}  ${hcode7}  ${hname}  ${WindowsMain}  ${WindowsPopup}
	Wait Until Page Contains Element  xpath://span[contains(text(), "เลือก")]
	Click Element  xpath://span[contains(text(), "เลือก")]
	Select Window  ${WindowsPopup}
	Wait Until Page Contains Element    id:hcode5
	Input Text    id:hcode5    ${hcode5}
	Wait Until Page Contains Element	id:hcode7
	Input Text    id:hcode7    ${hcode7}
	Wait Until Page Contains Element	id:hname
	Input Text    id:hname    ${hname}
	Click Element  id:search
	Wait Until Page Contains Element    //table[1]
	${rowIndex}  Get Row By Cell Text    //table[1]    11472    2
	Log To Console    \n rowIndex:${rowIndex}

	#rowsOfSearchResult  1
	#Select Window  name:${WindowsMain}
    #${rowIndex}  Get Row By Cell Text    ${CALENDAR_TABLE_ROW}    robot    2
	#Click Link In Table Cell    ${CALENDAR_TABLE_ROW}    ${rowIndex}    4    1

Get Table Row Count
	[Arguments]  ${rowLocator}
	Run Keyword And Return    Get Element Count    ${rowLocator}

Get Row By Cell Text
	[Arguments]  ${rowLocator}  ${cellText}  ${column}
	${rowCount}  Get Table Row Count  ${rowLocator}
	Log To Console    \n rowCount:${rowCount}
	:FOR  ${rowIndex}  IN RANGE  1  ${rowCount}+1
	      ${curText}  Get Text    ${rowLocator}[${rowIndex}]/td[${column}]
		  Exit For Loop  '${curText}'=='${cellText}'
	${rowNumber}  Set Variable    ${rowIndex}

Click Link In Table Cell
	[Arguments]  ${rowLocator}  ${row}  ${column}  ${index}
	Click Element    ${rowLocator}[${row}]/td[${column}]/a[${index}]
