*** Settings ***
Library  SeleniumLibrary
Resource  ../resources/nchConfiguration.robot
Suite Setup     gotoProject&SelectModule  admin  admin  Administrator/Import File Banks
Suite Teardown  Close Browser
Test Setup  Go To  ${NCHSYS0102}

*** Variables ***


*** Test Cases ***
NCHSYS0102A_Nor_Admin_Search_on Condition_Found
	[Tags]  Unit  UAT  Flow
	Input Condition for Search   ayuwadee  ${EMPTY}
	rowsOfSearchResult  1

NCHSYS0102A_Neg_Admin_Search_on Condition_Not Found
	Input Condition for Search   ayiwadee  ${EMPTY}
	SearchNotFound

*** Keywords ***
Input Condition for Search
    [Arguments]  ${userId}  ${userName}
	Wait Until Page Contains Element    id:userId
	Input Text    id:userId    ${userId}
	Wait Until Page Contains Element	id:userName
	Input Text    id:userName    ${userName}
    Click Element  id:search
